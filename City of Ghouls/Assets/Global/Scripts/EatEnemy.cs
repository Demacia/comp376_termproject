﻿using UnityEngine;
using System.Collections;

public abstract class EatEnemy : MonoBehaviour 
{
	private string DIDNT_EAT_GHOUL 	= "You feel stronger! Your stats have increased.";
	private string ATE_GHOUL 		= "You feel refreshed! Your HP is restored.";
	private string ATE_GHOUL_FULL 	= "Your HP is already full! Your stats have increased instead.";

	private string YES 	= "Restore full HP!";
	private string NO	= "Gain extra stats!";

	private bool decisionMade;
	private bool isDone;

	Combatant characterInPlay;
	GameObject textYes;
	GameObject textNo;

	GameObject buttonYes;
	GameObject buttonNo;

	void Awake()
	{
		characterInPlay = this.GetComponent<Combatant>();
	}

	public IEnumerator Run() 
	{
		CreateBackground();

		while(!isDone)
		{
			yield return StartCoroutine(PromptPlayer());
		}

		DestroyBackground();
		//Small delay to let the cutscene bands leave the screen - if there is a dialog right after, they won't appear.
		yield return new WaitForSeconds(1.0f);
		ResetVariables();
	}

	private IEnumerator PromptPlayer()
	{
		yield return StartCoroutine(DecisionMaker());
	}

	private void DestroyBackground()
	{
		SceneFactory sf = SceneFactory.instance;
		sf.FinishCutScene();
		DestroyDecisionButtons();
		DisplayText("");
	}

	private void DestroyDecisionButtons()
	{
		Destroy(buttonNo);
		Destroy(buttonYes);

		Destroy(textNo);
		Destroy(textYes);
	}

	protected virtual void CreateBackground()
	{
		SceneFactory sf = SceneFactory.instance;
		sf.StartCutScene(true);
		DisplayDecisionButtons();
		DisplayDecisionButtonsText();
	}

	private void DisplayDecisionButtons()
	{
		Vector3 noPosition = new Vector3(0.6f, 0.7f, 1.0f);
		Vector3 yesPosition = new Vector3(0.4f, 0.7f, 1.0f);

		buttonNo = (GameObject)Instantiate(GameObject.Find("EatGhoulButtonNo"), noPosition, Quaternion.identity);
		buttonYes = (GameObject)Instantiate(GameObject.Find("EatGhoulButtonYes"), yesPosition, Quaternion.identity);

		buttonNo.GetComponent<GUITexture>().enabled = true;
		buttonYes.GetComponent<GUITexture>().enabled = true;
	}

	private void DisplayDecisionButtonsText()
	{
		CreateTextObjects();
		StyleTexts();
	}

	private void CreateTextObjects()
	{
		textNo = new GameObject();
		textNo.name = "NoDecisionText";
		textYes = new GameObject();
		textYes.name = "YesDecisionText";


		textNo.AddComponent<GUIText>();
		textYes.AddComponent<GUIText>();
	}

	private void StyleTexts()
	{
		GUIText noGT = textNo.GetComponent<GUIText>();
		GUIText yesGT = textYes.GetComponent<GUIText>();

		Vector3 noPosition = new Vector3(0.6f, 0.5f, 1.0f);
		Vector3 yesPosition = new Vector3(0.4f, 0.5f, 1.0f);

		StyleText(noGT, noPosition);
		StyleText (yesGT, yesPosition);

		noGT.text = NO;
		yesGT.text = YES;
	}

	private void StyleText(GUIText gt, Vector3 position)
	{
		gt.color = Color.yellow;
		gt.fontSize = TextScaling.GetTextScale(20);
		gt.gameObject.transform.position = position;

		gt.alignment = TextAlignment.Center;
		gt.anchor = TextAnchor.MiddleCenter;
	}

	protected void DisplayText(string str)
	{
		SceneTextFactory.instance.DisplayText(str);
	}

	private IEnumerator DecisionMaker()
	{

		while(!decisionMade)
		{
			if(Input.GetKeyDown(KeyCode.Y))
			{
				EndDecisionMaker();
				EatGhoul();
				yield return new WaitForSeconds(2.0f);
			}
			
			if(Input.GetKeyDown(KeyCode.N))
			{
				EndDecisionMaker();
				DontEatGhoul();
				yield return new WaitForSeconds(2.0f);
			}
			yield return null;
		}

	}

	private void EatGhoul()
	{
		if(isHPFull())
		{
			DisplayText(ATE_GHOUL_FULL);
			IncreaseStats();
		}
		else
		{
			DisplayText(ATE_GHOUL);
			int fullHP = characterInPlay.level.stats.totalHP;
			characterInPlay.level.stats.currentHP = fullHP;
		}
        PlayerData.instance.UpdateMorality(-1);
	}

	private bool isHPFull()
	{
		return characterInPlay.level.stats.totalHP == characterInPlay.level.stats.currentHP;
	}

	private void DontEatGhoul()
	{
		DisplayText(DIDNT_EAT_GHOUL);
		IncreaseStats();
        PlayerData.instance.UpdateMorality(1);
	}

	private void IncreaseStats()
	{
		characterInPlay.level.stats.str++;
		characterInPlay.level.stats.dex++;
		characterInPlay.level.stats.lck++;
		characterInPlay.level.stats.totalHP++;
	}

	private void ResetVariables()
	{
		decisionMade = false;
		isDone = false;
	}

	private void EndDecisionMaker()
	{
		decisionMade = true;
		isDone = true;
	}
}
