﻿using UnityEngine;
using System.Collections.Generic;

public class Pools : MonoBehaviour 
{
	public static string MENU_BUTTON = "MenuButtons";
    public static string TARGETS = "Targets";
    public static string DMG_TICKS = "DmgTicks";
    public static string HP_TICKS = "HPTicks";
    public static string STAT_BOXES = "StatBoxes";
    public static string MATCHUP_BOXES = "MatchupBoxes";
    public static string LEVELUP_BOXES = "LevelupBoxes";

	private static Pools instance;

	private Dictionary<string, Pool> pools = new Dictionary<string, Pool>();

	void Awake () 
	{
		Pool[] p = GetComponentsInChildren<Pool>();
		for(int i = 0; i < p.Length; i++)
		{
			pools.Add (p[i].label, p[i]);
		}

		instance = this;
	}

	public static Pool GetPool(string label)
	{
		return instance.pools[label];
	}
}
