using UnityEngine;
using System.Collections;

public class BattleController : MonoBehaviour 
{
    public static BattleController instance;
    
    private BattleHealth attackerHealth;
    private BattleHealth defenderHealth;
    
    private BattleLabels attackerLabels;
    private BattleLabels defenderLabels;
    
    private Camera battleCamera;
    private Camera boardCamera;

    private ExpBox experience;
    // :)
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject); // Quang is the best!
        } 
        else
        {
            instance = this;
        
            boardCamera = Camera.main;
            battleCamera = transform.Find("BattleCamera").GetComponent<Camera>();
        
            // Get all the attacker details
            Transform a = transform.Find("Attacker");
            attackerHealth = a.Find("Health").GetComponent<BattleHealth>();
            attackerLabels = a.GetComponent<BattleLabels>();
        
            Transform d = transform.FindChild("Defender");
            defenderHealth = d.Find("Health").GetComponent<BattleHealth>();
            defenderLabels = d.GetComponent<BattleLabels>();

            experience = transform.Find("ExpBox").GetComponent<ExpBox>();
        }
    }
    
    private void OnDestroy()
    {
        if(instance == this)
        {
            instance = null;
        }
    }

    public void SetUp(Combatant attacker, Combatant defender)
    {
        AudioManager.instance.PlayCombat();
        battleCamera.enabled = true;
        boardCamera.enabled = false;

        attacker.transform.position = transform.position + (transform.forward * 10) + (-transform.right * 4f) + (-transform.up * attacker.sprite.transform.localPosition.y);
        defender.transform.position = transform.position + (transform.forward * 10) + (transform.right * 6f) + (-transform.up * attacker.sprite.transform.localPosition.y);

        attacker.sprite.transform.rotation = this.transform.rotation;
        defender.sprite.transform.rotation = this.transform.rotation;

        attackerLabels.SetCombatant(attacker);
        defenderLabels.SetCombatant(defender);
        attackerLabels.SetLabels(CalculateDamage(attacker, defender, false), HitPercentage(attacker, defender), CritPercentage(attacker));
        defenderLabels.SetLabels(CalculateDamage(defender, attacker, false), HitPercentage(defender, attacker), CritPercentage(defender));

        // Set the health bars and show them on screen
        attackerHealth.SetHealth(attacker.level.stats.totalHP, attacker.level.stats.currentHP);
        defenderHealth.SetHealth(defender.level.stats.totalHP, defender.level.stats.currentHP);
    }

    /// <summary>
    /// Checks to see if the attacker's weapon type has a/an (dis)advantage 
    /// on the defender's weapon type and returns a multiplier. 
    /// If the attacker is at an advantage, the damage is multiplied by 1.5.
    /// If the attacker is at a disadvantage, the damage is multiplied by 2/3.
    /// Otherwise, the multiplier is 1 (no change in damage).
    /// </summary>
    /// <param name="a">The attacker's weapon.</param>
    /// <param name="d">The defender's weapon.</param>
    /// <returns>Damage multipler based on attacker's weapon (dis)advantage</returns>
    public float ApplyRockPaperScizors(Weapon a, Weapon d)
    {
        float damageMultiplier = 1.0f;
        
        if(a.weaponClass == "attack"  && d.weaponClass == "defense"
           || a.weaponClass == "defense" && d.weaponClass == "speed"
           || a.weaponClass == "speed"   && d.weaponClass == "attack")
        {
            damageMultiplier = 3.0f/2.0f;
        }
        else if(d.weaponClass == "attack"  && a.weaponClass == "defense"
                || d.weaponClass == "defense" && a.weaponClass == "speed"
                || d.weaponClass == "speed"   && a.weaponClass == "attack")
        {            
            damageMultiplier = 2.0f/3.0f;
        }
        
        return damageMultiplier;
    }
    
    public int CalculateDamage(Combatant att, Combatant def, bool includeCrit)
    {
        // Calculate the damage
        int weaponStrength = (int)(ApplyRockPaperScizors(att.weapon, def.weapon) * att.weapon.attack);
        int totalStrength = att.level.stats.str + weaponStrength;
        int totalDefense = def.level.stats.def + def.weapon.def;
        
        float critMultiplier = 1;
        if(includeCrit)
        {
            // crit chance is from 0 - 1
            critMultiplier = (UnityEngine.Random.value < CritPercentage(att)) ? 1.5f : 1f;
        }

        int damageTaken = Mathf.FloorToInt(Mathf.Max(0, totalStrength - totalDefense) * critMultiplier);
        return damageTaken;
    }

    public IEnumerator AddExp(int currentExp, int expGain)
    {
        yield return StartCoroutine(experience.AddExp(currentExp, expGain));
    }
    // hello
    public void ReturnToGridView()
    {
        // Go back to the board camera and disable the health bars
        boardCamera.enabled = true;
        battleCamera.enabled = false;
    }

    public IEnumerator DecreaseDefenderHealth(int amount)
    {
        yield return StartCoroutine(defenderHealth.DecreaseHealth(amount));
    }

    /// <summary>
    /// Find the percent chance an attacker has to successfully hit the defender
    /// </summary>
    /// <returns>The percentage from 0 - 1 to 2 decimal places.</returns>
    public float HitPercentage(Combatant attacker, Combatant defender)
    {
        float hitPercent = Mathf.Clamp01(0.95f + (0.05f * (attacker.level.stats.dex - defender.level.stats.dex)));
        return (Mathf.Round(hitPercent * 100f) / 100f);
    }

    /// <summary>
    /// Find the percent chance an attacker has to critically strike
    /// </summary>
    /// <returns>The percentage from 0 - 1 to 2 decimal places.</returns>
    public float CritPercentage(Combatant attacker)
    {
        float totalCritChance = Mathf.Clamp01(attacker.level.stats.lck * 0.025f);
        return (Mathf.Round(totalCritChance * 100f) / 100f);
    }
}
// <3
