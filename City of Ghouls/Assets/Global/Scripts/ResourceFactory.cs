using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResourceFactory 
{
    static Dictionary<string, GameObject> resources = new Dictionary<string, GameObject>();
    static Dictionary<string, AudioClip> audioClips = new Dictionary<string, AudioClip>();

    public static GameObject SpawnCharacter(string name, int row, int col)
    {
        GameObject character;
        if(!resources.TryGetValue(name, out character))
        {
            character = Resources.Load<GameObject>("Characters/" + name);
            resources.Add(name, character);
        }

        Grid grid = MapOutline.instance.GetGrid(row, col);
        GameObject clone = Object.Instantiate(character, grid.transform.position, Quaternion.identity) as GameObject;

        GridMovement movement = clone.GetComponent<GridMovement>();
        movement.Row = row;
        movement.Col = col;

        PlayerData.instance.LoadCharacter(movement.combatant);

        grid.Occupant = movement;
        return clone;
    }

    public static void SpawnDirector(int row, int col)
    {
        GameObject director;
        if(!resources.TryGetValue("Director", out director))
        {
            director = Resources.Load<GameObject>("Global/Director");
            resources.Add("Director", director);
        }

        Grid grid = MapOutline.instance.GetGrid(row, col);
        GameObject clone = Object.Instantiate(director, grid.transform.position, Quaternion.identity) as GameObject;

        Director d = clone.GetComponent<Director>();
        d.Row = row;
        d.Col = col;
    }

    public static void LoadGlobalItems()
    {
        GameObject resource;
        if(!resources.TryGetValue("Pools", out resource))
        {
            resource = Resources.Load<GameObject>("Global/Pools");
            resources.Add("Pools", resource);
        }
        Object.Instantiate(resource);

        if(!resources.TryGetValue("BattleScene", out resource))
        {
            resource = Resources.Load<GameObject>("Global/BattleScene");
            resources.Add("BattleScene", resource);
        }
        Object.Instantiate(resource);

        if(!resources.TryGetValue("EnemyAI", out resource))
        {
            resource = Resources.Load<GameObject>("Global/EnemyAI");
            resources.Add("EnemyAI", resource);
        }
        Object.Instantiate(resource);

        if(!resources.TryGetValue("GUI", out resource))
        {
            resource = Resources.Load<GameObject>("Global/GUI");
            resources.Add("GUI", resource);
        }
        Object.Instantiate(resource);


		if(!resources.TryGetValue ("CutScenes", out resource))
		{
			resource = Resources.Load<GameObject>("Global/CutScenes");
			resources.Add ("CutScenes", resource);
		}
		Object.Instantiate(resource);
    }

    public static GameObject GetCutsceneItem(string itemName)
    {
        GameObject resource;
        if(!resources.TryGetValue("CutScene/" + itemName, out resource))
        {
            resource = Resources.Load<GameObject>("CutScene/" + itemName);
            resources.Add("CutScene/" + itemName, resource);
        }
        return resource;
    }

    public static AudioClip LoadAudio(string soundEffect)
    {
        AudioClip audio;
        if (!audioClips.TryGetValue(soundEffect, out audio))
        {
            audio = Resources.Load<AudioClip>(soundEffect);
            audioClips.Add(soundEffect, audio);
        }
        return audio;
    }
}
