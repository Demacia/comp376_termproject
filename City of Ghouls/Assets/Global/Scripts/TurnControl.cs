﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TurnControl : MonoBehaviour 
{
    public static TurnControl instance;

    public List<IObjective> objectives { get; private set; }

    private bool pauseTurns;
    private string currentTurn;

    public Dictionary<string, List<GridMovement>> characters { get; private set; }
    private GUIText turnIndicator;

	private void Awake () 
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        } 
        else
        {
            instance = this;

            characters = new Dictionary<string, List<GridMovement>>();
            turnIndicator = GameObject.FindGameObjectWithTag("GUI")
                                  .transform.FindChild("TurnIndicator").GetComponent<GUIText>();

            List<GridMovement> chars;
            characters.Add("Player", new List<GridMovement>());
            characters.TryGetValue("Player", out chars);
            GameObject[] gos = GameObject.FindGameObjectsWithTag("Player");
            for (int i = 0; i < gos.Length; i++)
            {
                chars.Add(gos [i].GetComponent<GridMovement>());
            }

            characters.Add("Enemy", new List<GridMovement>());
            characters.TryGetValue("Enemy", out chars);
            gos = GameObject.FindGameObjectsWithTag("Enemy");
            for (int i = 0; i < gos.Length; i++)
            {
                chars.Add(gos [i].GetComponent<GridMovement>());
            }

            pauseTurns = false;
            objectives = new List<IObjective>();
            currentTurn = "Player";
        }
	}
	
    private void OnDestroy()
    {
        if(instance == this)
        {
            instance = null;
        }
    }

    public void EnableTurns()
    {
        pauseTurns = false;
        EndCharacterTurn(currentTurn);
    }

    public void DisableTurns()
    {
        pauseTurns = true;
        Director.instance.enabled = false;
    }

    public void EndCharacterTurn(string tag)
    {
        currentTurn = tag;
        if (pauseTurns)
        {
            return;
        }

        if (objectives.Count > 0 && objectives.TrueForAll((o) => {return o.IsComplete();}))
        {
            pauseTurns = true;
            EndMission();
            return;
        }

        List<GridMovement> chars;
        characters.TryGetValue(tag, out chars);
        for(int i = 0; i < chars.Count; i++)
        {
            if(chars[i].moveAvailable) // If any character has an available move, do not change to the next phase
            {
                if(tag == "Player")
                {
                    Director.instance.enabled = true;
                }
                return;
            }
        }

        // If no characters have available moves, give all players back their moves and change turn
        foreach(List<GridMovement> gm in characters.Values)
        {
            for(int i = 0; i < gm.Count; i++)
            {
                gm[i].moveAvailable = true;
            }
        }

        //Change to next person's turn (player->enemy->player) or game over
		DecideWhosTurn(tag);
    }

	public bool AreAllEnemiesDead()
	{
		List<GridMovement> enemyChars;
		characters.TryGetValue("Enemy", out enemyChars);

		return enemyChars.Count == 0;
	}

	private void DecideWhosTurn(string tag)
	{
		List<GridMovement> enemyChars;
		characters.TryGetValue("Enemy", out enemyChars);

		List<GridMovement> playerChars;
		characters.TryGetValue("Player", out playerChars);

		if(playerChars.Count == 0)
		{
			PlayerCharsDead();
		}
		else if(enemyChars.Count == 0)
		{
			EnemyCharsDead();
		}
		else
		{
			StartCoroutine(ShowTurn(tag));
		}
	}

	public void PlayerCharsDead()
	{
		Director.instance.enabled = false;
		turnIndicator.text = "Game Over";
		turnIndicator.enabled = true;
        StartCoroutine(RestartLevelDelayed());
	}

	public void EnemyCharsDead()
	{
		StartCoroutine(ShowTurn("Enemy"));
	}

    private IEnumerator ShowTurn(string tag)
    {
        if(tag == "Player")
        {
            tag = "Enemy";
            Director.instance.enabled = false;
        }
        else if(tag == "Enemy")
        {
            tag = "Player";
            Director.instance.enabled = true;
        }
        turnIndicator.text = tag + "'s Turn";
        currentTurn = tag;

        turnIndicator.enabled = true;
        yield return new WaitForSeconds(1.25f);
        turnIndicator.enabled = false;

        if(tag == "Enemy")
        {
            EnemyAI.instance.StartTurn();
        }
    }

	public void RemoveCharacter(GridMovement gm, string tag)
	{
		List<GridMovement> chars;
		characters.TryGetValue(tag, out chars);
		chars.Remove(gm);
	}

    private void EndMission()
    {
        int levels = Application.levelCount;
        int nextLevel = (Application.loadedLevel + 1) % levels;

        if (nextLevel != 0)
        {
            PlayerData.instance.UpdateLevel(Application.loadedLevel + 1);
        }
        List<GridMovement> c;
        if (characters.TryGetValue("Player", out c))
        {
            for(int i = 0; i < c.Count; i++)
            {
                PlayerData.instance.UpdateCharacter(c[i].combatant.level);
            }
        }
        PlayerData.instance.Save();

        Application.LoadLevel(nextLevel);
    }

    private IEnumerator RestartLevelDelayed()
    {
        yield return new WaitForSeconds(3.0f);
        Application.LoadLevel(Application.loadedLevel);
    }
}
