using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;

[XmlRoot("Data")]
public class Data
{
    [XmlArray("Characters")]
    [XmlArrayItem("Character")]
    public HashSet<LevelData> characters = new HashSet<LevelData>();
    
    [XmlElement("Level")]
    public int level;

    [XmlElement("Morality")]
    public int morality;

    public void AddCharacter(LevelData character)
    {
        if (characters.Contains(character))
        {
            characters.Remove(character);
        }
        characters.Add(character);
    }
    
    public void Save()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Data));
        FileStream stream = new FileStream(Application.persistentDataPath + "/Data.dat", FileMode.Create);
        serializer.Serialize(stream, this);
        stream.Close();
        
        Debug.Log("Saved file to " + Application.persistentDataPath + "/Data.dat");
    }

    public static Data Load()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Data));
        Data data = new Data();
        try
        {
            FileStream stream = new FileStream(Application.persistentDataPath + "/Data.dat", FileMode.Open);
            data = serializer.Deserialize(stream) as Data;
            stream.Close();
            Debug.Log("Loaded file from " + Application.persistentDataPath + "/Data.dat");
        }
        catch(FileNotFoundException)
        {
            Debug.Log("No such file " + Application.persistentDataPath + "/Data.dat");
        }
        
        return data;
    }
    
    public class LevelData
    {
        [XmlAttribute("Hp")]
        public int hp;
        [XmlAttribute("Str")]
        public int str;
        [XmlAttribute("Dex")]
        public int dex;
        [XmlAttribute("Lck")]
        public int lck;
        [XmlAttribute("Exp")]
        public int exp;
        [XmlAttribute("Level")]
        public int level;
        [XmlAttribute("Key")]
        public string key;
        
        public override bool Equals(System.Object o)
        {
            if(o.GetType().Equals(this.GetType()))
            {
                return this.key == ((LevelData)o).key;
            }
            return false;
        }
        
        public override int GetHashCode()
        {
            return key.GetHashCode();
        }
    }

    public void Clear()
    {
        characters.Clear();
        level = 0;
        morality = 0;
    }
}
