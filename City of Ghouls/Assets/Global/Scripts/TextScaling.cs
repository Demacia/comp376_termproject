﻿using UnityEngine;
using System.Collections;

public static class TextScaling
{
	static int defaultScreenWidth = 1024;

	public static int GetTextScale(int currentTextSize = 22)
	{
		return Mathf.RoundToInt (currentTextSize * Screen.width / (defaultScreenWidth * 1.0f));
	}
}
