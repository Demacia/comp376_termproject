
using System;
using UnityEngine;
public class EatEnemyHuman : EatEnemy
{
	private string EAT_GHOUL_PROMPT_HUMANS = "Would you like to kill this beaten Ghoul?";
	
	protected override void CreateBackground ()
	{
		base.CreateBackground ();
		base.DisplayText(EAT_GHOUL_PROMPT_HUMANS);
	}
}
