﻿using UnityEngine;
using System.Collections;

public static class DisableCharacter
{
	public static void Disable(GameObject o) 
	{
        if (o.tag == "Player")
        {
            o.GetComponent<AfterMovementContext>().enabled = false;
            o.GetComponent<EatEnemy>().enabled = false;
        }
        o.GetComponent<Level>().enabled = false;
        o.GetComponent<Weapon>().enabled = false;
        o.GetComponent<Combatant>().enabled = false;
        o.tag = "Untagged";
	}

	public static void LevelUp(GameObject[] o, int experience) 
	{
		for(int i = 0; i < o.Length; i++)
		{
			o[i].GetComponent<Level>().AddExperience(experience);
		}
	}
}
