﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyAI : MonoBehaviour 
{
    public static EnemyAI instance;

    private List<GridMovement> units = new List<GridMovement>();
    private List<GridMovement> playerUnits = new List<GridMovement>();

	private void Awake () 
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        instance = this;

        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        for(int i = 0; i < enemies.Length; i++)
        {
            GridMovement e = enemies[i].GetComponent<GridMovement>();
            units.Add(e);
        }

        GameObject[] player = GameObject.FindGameObjectsWithTag("Player");
        for(int i = 0; i < player.Length; i++)
        {
            GridMovement p = player[i].GetComponent<GridMovement>();
            playerUnits.Add(p);
        }
	}
	
	public void StartTurn()
    {
        PerformMove(0);
    }

    /// <summary>
    /// Move one unit around the map and attack if possible.
    /// </summary>
    /// <param name="index">Index in the enemy unit list.</param>
    private void PerformMove(int index)
    {
		if(ArePlayerCharsDead())
		{
			TurnControl.instance.PlayerCharsDead();
		}
		else if(index < units.Count)
        {
            units[index].Select();
            LinkedList<Grid> path = FindPath(units[index]);
            units[index].MoveTo(path);
            StartCoroutine(WaitForUnit(units[index], index));
        }
    }

    /// <summary>
    /// Finds a path that leads towards a player character
    /// </summary>
    /// <returns>The target grid that will bring the unit closer to the player</returns>
    private LinkedList<Grid> FindPath(GridMovement start)
    {
        // Find the closest player unit
        GridMovement closestUnit = playerUnits[0];
        for(int i = 1; i < playerUnits.Count; i++)
        {
            GridMovement otherUnit = playerUnits[i];
            if(IsCloser(start, otherUnit, closestUnit))
            {
                closestUnit = otherUnit;
            }
        }

        Grid from = MapOutline.instance.GetGrid(start.Row, start.Col);
        Grid destination = MapOutline.instance.GetGrid(closestUnit.Row, closestUnit.Col);
        List<Grid> path = from.FindPath(destination);
        LinkedList<Grid> enemyPath = new LinkedList<Grid>();
        for(int i = 0; i < path.Count && i < start.movementRange + 1; i++)
        {
            if(!path[i].IsOccupied() || path[i].Occupant == start)
            {
                enemyPath.AddLast(path[i]);
            }
        }

        return enemyPath;
    }

    /// <summary>
    /// Determines whether this instance is closer to the compare or the old.
    /// </summary>
    /// <returns><c>true</c> if this instance is closer to compare; otherwise, <c>false</c>.</returns>
    private bool IsCloser(GridMovement start, GridMovement compare, GridMovement old)
    {
        Grid s = MapOutline.instance.GetGrid(start.Row, start.Col);
        Grid c = MapOutline.instance.GetGrid(compare.Row, compare.Col);
        Grid o = MapOutline.instance.GetGrid(old.Row, old.Col);
        if(s.Distance(c) < s.Distance(o))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Wait for the unit to finish moving and attacking and then move the next unit
    /// </summary>
    /// <param name="unit">Unit.</param>
    /// <param name="index">Index.</param>
    private IEnumerator WaitForUnit(GridMovement unit, int index)
    {
        while(unit.moveAvailable)
        {
            yield return new WaitForEndOfFrame();
        }

        PerformMove(index + 1);
    }

	public void RemoveCharacter(GridMovement gm)
	{
		if(units.Contains(gm))
		{
			units.Remove(gm);
		}
		else
		{
			playerUnits.Remove(gm);
		}
	}

	private bool ArePlayerCharsDead()
	{
		return playerUnits.Count == 0;
	}
}
