
using System;
using UnityEngine;
public class EatEnemyGhoul : EatEnemy
{
	private string EAT_GHOUL_PROMPT_GHOULS = "Would you like to eat this beaten Ghoul?";

	protected override void CreateBackground ()
	{
		base.CreateBackground ();
		base.DisplayText(EAT_GHOUL_PROMPT_GHOULS);
	}
}
