﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class PlayerData : MonoBehaviour 
{
    public static PlayerData instance;

    private Data data;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            data = Data.Load();
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void LoadLevel()
    {
        Application.LoadLevel(data.level);
    }

    public void LoadCharacter(Combatant c)
    {
        Data.LevelData levelData = null;
        foreach(Data.LevelData d in data.characters)
        {
            if(d.key == c.level.key)
            {
                levelData = d;
            }
        }
        if (levelData != null)
        {
            c.level.exp = levelData.exp;
            c.level.level = levelData.level;
            c.level.stats.totalHP = levelData.hp;
            c.level.stats.currentHP = levelData.hp;
            c.level.stats.str = levelData.str;
            c.level.stats.lck = levelData.lck;
        }
    }

    public void UpdateCharacter(Level level)
    {
        // Convert to serializable class
        Data.LevelData l = new Data.LevelData();
        l.key = level.key;
        l.level = level.level;
        l.exp = level.exp;
        l.lck = level.stats.lck;
        l.dex = level.stats.dex;
        l.str = level.stats.str;
        l.hp = level.stats.totalHP;

        data.AddCharacter(l); // Save
    }

    public void UpdateMorality(int modifier)
    {
        data.morality += modifier;
    }

    public void UpdateLevel(int level)
    {
        data.level = level;
    }

    public void Save()
    {
        data.Save();
    }

    public void Delete()
    {
        data.Clear();
    }

    public bool IsGood()
    {
        return data.morality > -1;
    }
}
