﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Director : MonoBehaviour 
{
    public static Director instance;

    // Character that is being commanded
    private GameObject selectedCharacter;
     
    // Grid coordinates of director
    public int Row;
    public int Col;

    private float movementPeriod = 0.0f;

    // Selector GUI
    private Renderer selectorRenderer;
    private LineRenderer pathVisual;

    // Keep the path that the player makes
    private LinkedList<Grid> path = new LinkedList<Grid>();

    private GameObject activeStats;

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        instance = this;

        selectorRenderer = this.transform.FindChild("Selector").gameObject.renderer;
        pathVisual = this.transform.FindChild("Path").GetComponent<LineRenderer>();

        Camera.main.transform.parent = this.transform;
        Camera.main.isOrthoGraphic = true;
        Camera.main.orthographicSize = 5;
        Camera.main.transform.localPosition = new Vector3(6, 6, -6);
        Camera.main.transform.rotation = Quaternion.Euler(30, -45, 0);
    }
	
	private void Update () 
	{
	    if(Input.GetButtonDown("Select"))
        {
			Grid target = MapOutline.instance.GetGrid(Row, Col);
            if(target.IsOccupied() && target.Occupant.gameObject != selectedCharacter) // If selecting another character, switch the selected character
            {
                if(target.Occupant.tag == "Player")
                {
                    if(!target.Occupant.moveAvailable) // If the character to switch to already moved, do nothing
                    {
                        return;
                    }
                    if(selectedCharacter != null)
                    {
                        selectedCharacter.SendMessage("UnSelect");
                    }
                    selectedCharacter = target.Occupant.gameObject;
                    selectedCharacter.SendMessage("Select");

                    path.Clear();
                    pathVisual.SetVertexCount(0);
                    path.AddFirst(target);
                }
                else if(target.Occupant.tag == "Enemy" && selectedCharacter != null)
                {
                    selectedCharacter.SendMessage("MoveAndAttack", path);
                    ResetState();
                }
            }
            else if(selectedCharacter != null) // If a character is selected, try to move it
            {
                selectedCharacter.SendMessage("MoveTo", path);
                ResetState();
            }
        }
        else if(Input.GetButtonDown("Unselect"))
        {
            if(selectedCharacter != null)
            {
                selectedCharacter.SendMessage("UnSelect");
                ResetState();
            }
        }
        else if(Input.GetButton("Horizontal"))
        {
            if(Input.GetButtonDown("Horizontal"))
            {
                movementPeriod = 0.5f;
            }
            else if(movementPeriod > 0)
            {
                movementPeriod -= Time.deltaTime;
                return;
            }
            else
            {
                movementPeriod = 0.1f;
            }
            float horizontal = Input.GetAxisRaw("Horizontal");
            Col += (int)horizontal;
            Grid grid = MapOutline.instance.GetGrid(Row, Col);
            if(grid != null)
            {
                this.transform.position = grid.transform.position;
                DisplayStats();
                FindPath(grid);
            }
            else
            {
                Col -= (int)horizontal;
            }
        }
        else if(Input.GetButton("Vertical"))
        {
            if(Input.GetButtonDown("Vertical"))
            {
                movementPeriod = 0.5f;
            }
            else if(movementPeriod > 0)
            {
                movementPeriod -= Time.deltaTime;
                return;
            }
            else
            {
                movementPeriod = 0.1f;
            }
            float vertical = Input.GetAxisRaw("Vertical");
            Row += (int)vertical;
            Grid grid = MapOutline.instance.GetGrid(Row, Col);
            if(grid != null)
            {
                this.transform.position = grid.transform.position;
                DisplayStats();
                FindPath(grid);
            }
            else
            {
                Row -= (int)vertical;
            }
        }
	}

    private void FindPath(Grid destination)
    {
        if(selectedCharacter != null && destination.IsActive)
        {
            Grid from = path.First.Value;
            if(destination.IsOccupied() && destination != from) // Do not modify the path if the grid is occupied and it is not the start
            {
                return;
            }

            path.Clear();
            List<Grid> tempPath = from.FindPath(destination); // Must be converted to a linked list
            for(int i = 0; i < tempPath.Count; i++)
            {
                path.AddLast(tempPath[i]);
            }

            pathVisual.SetVertexCount(path.Count);
            LinkedListNode<Grid> pathNode = path.First;
            for(int i = 0; i < path.Count; i++)
            {
                pathVisual.SetPosition(i, pathNode.Value.transform.position);
                pathNode = pathNode.Next;
            }
        }
    }

    private void DisplayStats()
    {
        if (activeStats != null)
        {
            activeStats.SetActive(false);
        }

        GridMovement occupant = MapOutline.instance.GetGrid(Row, Col).Occupant;
        if (occupant != null)
        {
            GameObject statsBox = Pools.GetPool(Pools.STAT_BOXES).GetObject();
            statsBox.SendMessage("SetLabels", occupant);
            activeStats = statsBox;
        } 
        else
        {
            activeStats = null;
        }
    }

    private void ResetState()
    {
        pathVisual.SetVertexCount(0);
        selectedCharacter = null;
        if (activeStats != null)
        {
            activeStats.SetActive(false);
        }
    }

    private void OnDestroy()
    {
        if(instance == this)
        {
            instance = null;
        }
    }

    private void OnEnable()
    {
        selectorRenderer.enabled = true;
    }

    private void OnDisable()
    {
        selectorRenderer.enabled = false;
    }
}
