﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour 
{
    public static AudioManager instance;
    
    public AudioClip sceneClip;
    public AudioClip combatClip;
    public AudioClip gameClip;
    public AudioClip dialogClip;
    public AudioClip clickClip;
    
    AudioSource backgroundAudioSource;
    AudioSource soundEffectsAudioSource;
    
    void Awake () 
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        instance = this;
        
        backgroundAudioSource = GetComponent<AudioSource>();
        soundEffectsAudioSource = GameObject.FindGameObjectWithTag("SoundEffectsAudio").GetComponent<AudioSource>();
        PlayGameplay();
    }
    
    public void PlayScene()
    {
        backgroundAudioSource.Stop();
		backgroundAudioSource.volume = 0.5f;
        backgroundAudioSource.clip = sceneClip;
        backgroundAudioSource.Play();
    }
    
    public void PlayCombat()
    {
        float volumeLevel = 1.0f;
        PlaySound(combatClip, volumeLevel, -1.0f);
    }
    
    public void PlayGameplay()
    {
        backgroundAudioSource.Stop();
        backgroundAudioSource.volume = 0.05f;
        backgroundAudioSource.clip = gameClip;
        backgroundAudioSource.Play();
    }
    
    public void PlayDialog()
    {
        float volumeLevel = 1.0f;
        PlaySound(dialogClip, volumeLevel, -1.0f);
    }
    
    public void PlayClick(float pitch)
    {
        float volumeLevel = 0.3f;
        PlaySound(clickClip, volumeLevel, -1.0f, 1f + pitch);
    }
    
    public void Stop()
    {
        backgroundAudioSource.Stop();
        PlayGameplay();
    }
    
    private void PlaySound(AudioClip clip, float volume, float stopAt, float pitch = 1f)
    {
        soundEffectsAudioSource.Stop();
        
        soundEffectsAudioSource.clip = clip;
        if(stopAt > 0.0f)
        {
            soundEffectsAudioSource.SetScheduledEndTime(stopAt);
        }
        soundEffectsAudioSource.volume = volume;
        soundEffectsAudioSource.pitch = pitch;
        soundEffectsAudioSource.Play();
    }

    public void PlaySoundEffect(string soundEffect)
    {
        float volumeLevel = 1.0f;
        AudioClip clip = ResourceFactory.LoadAudio(soundEffect);
        PlaySound(clip, volumeLevel, -1.0f);
    }
}
