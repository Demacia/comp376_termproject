﻿using UnityEngine;
using System.Collections;

public class ButtonManager: MonoBehaviour
{
	private string buttonName;

	public void SetName(string name)
	{
		buttonName = name;
	}

    public void Update()
    {
        if(Input.GetMouseButtonDown(0) && guiTexture.HitTest(Input.mousePosition))
        {
            PerformAction();
        }
    }

	void PerformAction()
	{
		switch(buttonName)
		{
		case "new":
			Application.LoadLevel(1);
            PlayerData.instance.Delete();
			break;
		case "options":
			MainMenu.instance.DisableMainMenu();
			MainMenu.instance.DisplayOptions();
			break;
		case "quit":
			Application.Quit();
			break;
		case "back":
			MainMenu.instance.DisableCurrentScreen();
			MainMenu.instance.DisplayMainMenu();
			break;
        case "load":
            PlayerData.instance.LoadLevel();
            break;
		}
	}

}
