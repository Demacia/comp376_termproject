﻿using UnityEngine;
using System.Collections.Generic;

public class MainMenu : MonoBehaviour 
{
	public static MainMenu instance;

	private Vector3 genericButtonPosition = new Vector3(0.05f, 0.2f, 1.0f);
	private Vector3 backButtonPosition = new Vector3(0.05f, 0.8f, 1.0f);

	List<GameObject> mainMenu;
	List<GameObject> options;

	private string currentScreen = "";

	void Awake()
	{
		if(instance != null)
		{
			Destroy(this.gameObject);
		}

		instance = this;

		mainMenu = new List<GameObject>();
		options = new List<GameObject>();
	}

	void Start()
	{
		CreateMainMenuButtons();
		CreateOptionsButtons();
		DisplayMainMenu();
	}

	public void DisplayMainMenu()
	{
		foreach(GameObject buttonClone in mainMenu)
		{
			buttonClone.SetActive(true);
		}

		currentScreen = "main";
	}

	public void DisableMainMenu()
	{
		foreach(GameObject buttonClone in mainMenu)
		{
			buttonClone.SetActive(false);
		}
	}

	public void DisplayOptions()
	{
		foreach(GameObject buttonClone in options)
		{
			buttonClone.SetActive(true);
		}

		currentScreen = "options";
	}
	
	public void DisableOptions()
	{
		foreach(GameObject buttonClone in options)
		{
			buttonClone.SetActive(false);
		}
	}

	void CreateMainMenuButtons()
	{
		ButtonFactory.instance.ResetButtonOffset();
		GameObject quit = ButtonFactory.instance.CreateNewButton(genericButtonPosition, "Quit", "quit");
		GameObject options = ButtonFactory.instance.CreateNewButton(genericButtonPosition, "Options", "options");
		GameObject loadGame = ButtonFactory.instance.CreateNewButton(genericButtonPosition, "Load Game", "load");
		GameObject newGame = ButtonFactory.instance.CreateNewButton(genericButtonPosition, "New Game", "new");

		AddGuiElements(mainMenu, new GameObject[] {newGame, loadGame, options, quit});
	}

	void CreateOptionsButtons()
	{
		ButtonFactory.instance.ResetButtonOffset();
		GameObject back = ButtonFactory.instance.CreateNewButton(backButtonPosition, "Back", "back");
		ButtonFactory.instance.ResetButtonOffset();
		GameObject volume = ButtonFactory.instance.CreateNewSlider(genericButtonPosition, "Volume", "volume");

		AddGuiElements(options, new GameObject[] {back, volume});
	}
	
	private bool AddGuiElements(List<GameObject> list, GameObject[] buttons)
	{
		foreach(GameObject buttonClone in buttons)
		{
			list.Add(buttonClone);
		}

		return true;
	}

	public void DisableCurrentScreen()
	{
		switch(currentScreen)
		{
		case "options":
			DisableOptions();
			break;
		case "new":
			//Disable new
			break;
		case "load":
			//Disable load
			break;
		}
	}

	//End
	/*
	public string levelToLoadWhenClickedPlay = "";
	public string[] aboutTextLines = new string[0];
	
	private string clicked = "", aboutMessage = "About \n ";
	private Rect buttonRect;
	private float volume = 1.0f;
	
	private void Start()
	{
		for (int i = 0; i < aboutTextLines.Length; i++)
		{
			aboutMessage += aboutTextLines[i] + " \n ";
		}
		aboutMessage += "Press Esc To Go Back";


		buttonRect = new Rect(0.0f, 0.0f, buttonHeight, buttonWidth);
	}
	
	private void OnGUI()
	{
		if (clicked == "")
		{
			buttonRect = GUI.Window(0, buttonRect, menuFunc, "Main Menu");
		}
		else if (clicked == "options")
		{
			buttonRect = GUI.Window(1, buttonRect, optionsFunc, "Options");
		}
		else if (clicked == "about")
		{
			GUI.Box(new Rect (0,0,Screen.width,Screen.height), aboutMessage);
		}else if (clicked == "resolution")
		{
			GUILayout.BeginVertical();
			for (int x = 0; x < Screen.resolutions.Length;x++ )
			{
				if (GUILayout.Button(Screen.resolutions[x].width + "X" + Screen.resolutions[x].height))
				{
					Screen.SetResolution(Screen.resolutions[x].width,Screen.resolutions[x].height,true);
				}
			}
			GUILayout.EndVertical();
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Back"))
			{
				clicked = "options";
			}
			GUILayout.EndHorizontal();
		}
	}
	
	private void optionsFunc(int id)
	{
		if (GUILayout.Button("Resolution"))
		{
			clicked = "resolution";
		}
		GUILayout.Box("Volume");
		volume = GUILayout.HorizontalSlider(volume ,0.0f,1.0f);
		AudioListener.volume = volume;
		if (GUILayout.Button("Back"))
		{
			clicked = "";
		}
	}
	
	private void menuFunc(int id)
	{
		//buttons 
		if (GUILayout.Button("Play Game"))
		{
			//play game is clicked
			Application.LoadLevel(1);
		}
		if (GUILayout.Button("Options"))
		{
			clicked = "options";
		}
		if (GUILayout.Button("About"))
		{
			clicked = "about";
		}
		if (GUILayout.Button("Quit Game"))
		{
			Application.Quit();
		}
	}
	
	private void Update()
	{
		if (clicked == "about" && Input.GetKey (KeyCode.Escape))
			clicked = "";
	}*/

}
