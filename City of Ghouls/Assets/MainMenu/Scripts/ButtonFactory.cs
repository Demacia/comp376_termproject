﻿using UnityEngine;
using System.Collections;

public class ButtonFactory : MonoBehaviour
{
	public static ButtonFactory instance;

	public GameObject button;

	float originalButtonOffset = 0.1f;
	float buttonOffset;
	
	float buttonHeight = Screen.height/10;
	float buttonWidth = Screen.width/3;

	void Awake () 
	{
		if(instance != null)
		{
			Destroy(this.gameObject);
		}
		instance = this;

		buttonOffset = originalButtonOffset;
	}

	public GameObject CreateNewSlider(Vector3 basePosition, string text, string name)
	{
		GameObject sliderClone = new GameObject();
		sliderClone.AddComponent("SliderManager");
		sliderClone.SetActive(false);
		return sliderClone;
	}
	
	public GameObject CreateNewButton(Vector3 basePosition, string text, string name)
	{
		GameObject buttonClone = (GameObject)Instantiate(button, GetButtonPosition(basePosition), Quaternion.identity);
		SetDimensions(buttonClone);
		SetColliderDimensions(buttonClone);
		SetText(buttonClone, text);
		SetName(buttonClone, name);
		buttonClone.SetActive(false);
		return buttonClone;
	}

	private void SetName(GameObject cloneButton, string name)
	{
		cloneButton.GetComponent<ButtonManager>().SetName(name);
	}

	private void SetColliderDimensions(GameObject buttonClone)
	{
		buttonClone.GetComponent<BoxCollider2D>().size = new Vector2(buttonWidth, buttonHeight);
	}

	private void SetDimensions(GameObject buttonClone)
	{
		GUITexture buttonGuiTexture = buttonClone.GetComponent<GUITexture>();
		
		buttonGuiTexture.pixelInset = new Rect(0.0f, 0.0f, buttonWidth, buttonHeight);
	}

	public void ResetButtonOffset()
	{
		buttonOffset = originalButtonOffset;
	}

	private Vector3 GetButtonPosition(Vector3 basePosition)
	{
		Vector3 position = new Vector3(basePosition.x, basePosition.y - buttonOffset, basePosition.z);
		buttonOffset -= originalButtonOffset;
		return position;
	}
	
	private void SetText(GameObject buttonClone, string text)
	{
		GUIText buttonText = buttonClone.GetComponentInChildren<GUIText>();
		buttonText.text = text;

		buttonText.pixelOffset = new Vector2(buttonWidth/10, buttonHeight/2f);
	}

}
