﻿using UnityEngine; 
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

public abstract class Combatant : MonoBehaviour 
{
    public string characterName;

    public Transform sprite {get; private set;}
    public Animator animator { get; private set; }
    public Weapon weapon {get; private set;}
    public Level level {get; private set;}
    public EatEnemy eatEnemy {get; private set;}

    protected GridMovement grid { get; private set; }

    protected List<Combatant> availableAttacks;

    private IBattleState battleState;

    protected virtual void Awake()
    {
        level = GetComponent<Level>();
        grid = this.GetComponent<GridMovement>();
        weapon = this.GetComponent<Weapon>();
        eatEnemy = this.GetComponent<EatEnemy>();

        sprite = this.transform.Find("Sprite");
        battleState = sprite.GetComponent<IBattleState>();
        animator = sprite.GetComponent<Animator>();

        availableAttacks = new List<Combatant>();

        // Use reflection to find the character name from the static class
        bool foundName = false;
        FieldInfo[] fields = typeof(CharacterNames).GetFields();
        for (int i = 0; i < fields.Length; i++)
        {
            if(fields[i].Name == characterName)
            {
                foundName = true;
                characterName = fields[i].GetValue(null).ToString(); // Static field so pass null
            }
        }

        if (!foundName)
        {
            throw new UnityException("Could not find name in CharacterNames");
        }
    }

    public abstract void SelectTarget();

    public IEnumerator Attack(Combatant defender)
    {
        Vector3 originalPosition = this.transform.position;
        Quaternion originalRotation = this.sprite.transform.rotation;
        
        Vector3 defenderPosition = defender.transform.position;
        Quaternion defenderRotation = defender.sprite.transform.rotation;
        
        battleState.opponent = defender;
        battleState.myself = this;

        BattleController.instance.SetUp(this, defender);

        yield return StartCoroutine(battleState.EngageState());
        yield return StartCoroutine(battleState.ActionState());
        yield return StartCoroutine(battleState.EndState());

        BattleController.instance.ReturnToGridView();
        
        this.transform.position = originalPosition;
        this.sprite.transform.rotation = originalRotation;

        if(defender != null) // Might have been killed
        {
            defender.transform.position = defenderPosition;
            defender.sprite.transform.rotation = defenderRotation;
        }

        Finished();
    }

    public virtual void Finished()
    {
        grid.Wait();
    }

	public virtual void Kill()
	{
		EnemyAI.instance.RemoveCharacter(grid);
		TurnControl.instance.RemoveCharacter(grid, this.gameObject.tag);

        Destroy(this.gameObject);
    }

    public bool AvailableAttacks()
    {
        availableAttacks.Clear();
        FindTargets(weapon.range, MapOutline.instance.GetGrid(grid.Row, grid.Col));
        
        return availableAttacks.Count > 0;
    }

    private void FindTargets(int range, Grid search)
    {
        List<Grid> grids = search.GetAdjacentGrids();
        foreach(Grid g in grids)
        {
            if(g.IsOccupied() && IsEnemy(g.Occupant))
            {
                Combatant c = g.Occupant.GetComponent<Combatant>();
                if(!availableAttacks.Contains(c))
                {
                    availableAttacks.Add(c);
                }
            }
            
            if(range > 1)
            {
                FindTargets(range - 1, g);
            }
        }
    }

    protected abstract bool IsEnemy(GridMovement g);

    public bool IsDead()
    {
        return level.stats.currentHP < 1;
    }
}
