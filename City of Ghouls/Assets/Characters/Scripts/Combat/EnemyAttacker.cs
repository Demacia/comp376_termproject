﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyAttacker : Combatant 
{
    public override void SelectTarget()
    {
        if(AvailableAttacks())
        {
            Combatant target = availableAttacks[UnityEngine.Random.Range(0, availableAttacks.Count)];
            StartCoroutine(ShowTargetSelected(target));
        }
        else
        {
            grid.Wait();
        }
    }

    private IEnumerator ShowTargetSelected(Combatant targetPlayer)
    {
        GameObject target = Pools.GetPool(Pools.TARGETS).GetObject();
        target.transform.position = targetPlayer.transform.position;
        yield return new WaitForSeconds(0.5f);
        target.SetActive(false);

        StartCoroutine(Attack(targetPlayer));
    }

    protected override bool IsEnemy(GridMovement g)
    {
        return g.tag == "Player";
    }
}
