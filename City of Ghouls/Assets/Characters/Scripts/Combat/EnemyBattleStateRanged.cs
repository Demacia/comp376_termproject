using UnityEngine;
using System.Collections;

public class EnemyBattleStateRanged : IBattleState 
{
    private ParticleSystem projectiles;
    
    protected override void Awake()
    {
        base.Awake();
        projectiles = transform.Find("Projectiles").particleSystem;
    }
    
    protected override IEnumerator QuickTimeEvent()
    {
        projectiles.Play();
        
        int finalDamage = 0;
        bool missed = UnityEngine.Random.value > BattleController.instance.HitPercentage(myself, opponent);
        
        if(!missed)
        {
            yield return StartCoroutine(opponent.weapon.PlayDefensiveMinigame());
            int damageTaken = BattleController.instance.CalculateDamage(myself, opponent, true);
            finalDamage = ((int)(damageTaken * opponent.weapon.percentSuccess));
        }
        else
        {
            yield return StartCoroutine(myself.weapon.Miss());
        }
        
        opponent.level.stats.currentHP -= finalDamage;
        
        yield return StartCoroutine(BattleController.instance.DecreaseDefenderHealth(finalDamage));
        
        projectiles.Stop();
    }
}
