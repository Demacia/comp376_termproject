using UnityEngine; 
using System.Collections;
using System.Collections.Generic;

public class PlayerAttacker : Combatant
{
    private int targetIndex;
    private GameObject target;

    private GameObject matchupBox;

    protected override void Awake()
    {
        base.Awake();
        this.enabled = false;
    }

    public void Update()
    {
        if(Input.GetButtonDown("Vertical"))
        {
            if(Input.GetAxis("Vertical") < 0)
            {
                targetIndex = (targetIndex + 1)%availableAttacks.Count;
            }
            else
            {
                targetIndex = (targetIndex - 1 + availableAttacks.Count)%availableAttacks.Count;
            }
            SwitchTargets();
        }
        else if(Input.GetButtonDown("Select"))
        {
            StartCoroutine(Attack(availableAttacks[targetIndex]));
            Reset();
        }
        else if(Input.GetButtonDown("Unselect"))
        {
            grid.CancelMovement();
            Reset();
        }
    }

    private void ShowMatchup()
    {
        if (matchupBox != null)
        {
            matchupBox.SetActive(false);
        }
        Combatant defender = availableAttacks[targetIndex];
        matchupBox = Pools.GetPool(Pools.MATCHUP_BOXES).GetObject();
        matchupBox.SendMessage("SetLabels", new Combatant[2] {this, defender});
    }

    private void SwitchTargets()
    {
        Combatant defender = availableAttacks[targetIndex];
        if (target != null)
        {
            target.SetActive(false);
        }
        target = Pools.GetPool(Pools.TARGETS).GetObject();
        target.transform.position = defender.transform.position;
        ShowMatchup();
    }

    public override void SelectTarget()
    {
        targetIndex = 0;
        SwitchTargets();
        this.enabled = true;
    }

    public override void Finished()
    {
        base.Finished();
    }

    private void Reset()
    {
        if (matchupBox != null)
        {
            matchupBox.SetActive(false);
        }

        target.SetActive(false);
        target = null;
        availableAttacks.Clear();
        this.enabled = false;
    }

    protected override bool IsEnemy(GridMovement g)
    {
        return g.tag == "Enemy";
    }
}
