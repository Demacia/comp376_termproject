using UnityEngine;
using System.Collections;

public class PlayerBattleStateRanged : IBattleState 
{
    private ParticleSystem projectiles;

    protected override void Awake()
    {
        base.Awake();
        projectiles = transform.Find("Projectiles").particleSystem;
    }

    protected override IEnumerator QuickTimeEvent()
    {
        projectiles.Play();

        int finalDamage = 0;
        bool missed = UnityEngine.Random.value > BattleController.instance.HitPercentage(myself, opponent);
        
        if(!missed)
        {
            int damageTaken = BattleController.instance.CalculateDamage(myself, opponent, true);
            yield return StartCoroutine(myself.weapon.PlayOffensiveMinigame());
            finalDamage = ((int)(damageTaken * myself.weapon.percentSuccess));
        }
        else
        {
            yield return StartCoroutine(myself.weapon.Miss());
        }
        
        opponent.level.stats.currentHP -= finalDamage;
        
        yield return StartCoroutine(BattleController.instance.DecreaseDefenderHealth(finalDamage));

        projectiles.Stop();
    }
}
