﻿using UnityEngine;
using System.Collections;

public abstract class IBattleState : MonoBehaviour 
{
    public Combatant opponent {get; set;}
    public Combatant myself {get; set;}

    protected Animator animator {get; private set;}

    private int originalOrder;
    private float approachDuration = 1.0f;

    protected virtual void Awake()
    {
        animator = this.GetComponent<Animator>();
    }

    public virtual IEnumerator EngageState()
    {
        originalOrder = this.renderer.sortingOrder;
        this.renderer.sortingOrder = opponent.animator.renderer.sortingOrder + 1;
        animator.SetBool("isIdle", false);
        opponent.animator.SetBool("isDefending", true);
        yield return new WaitForSeconds(0.5f); // Wait 0.5 seconds before starting

        animator.SetBool("isAttacking", true);
        yield return new WaitForSeconds(approachDuration);
    }

    public virtual IEnumerator ActionState()
    {
        yield return StartCoroutine(QuickTimeEvent());

        animator.SetBool("isAttacking", false);
        yield return new WaitForSeconds(approachDuration);
    }

    public virtual IEnumerator EndState()
    {
        // Check if attacker killed defender
        if(opponent.IsDead())
        {
            animator.SetTrigger("isVictorious");
            opponent.animator.SetTrigger("isDead");
            AudioManager.instance.PlaySoundEffect(SoundEffects.DeathEffect);

            // Player specific events
            if(myself.tag == "Player")
            {
                // Decide if you want to eat or not
                if(myself.eatEnemy != null)
                {
                    yield return StartCoroutine(myself.eatEnemy.Run());
                }

                // Add experience and show player
                int currentLevel = myself.level.level;
                Stats currentStats = myself.level.stats.Clone();

                int currentExp = myself.level.exp;
                int expGain = myself.level.AddExperienceBasedOnLevel(opponent.level.level);
                yield return StartCoroutine(BattleController.instance.AddExp(currentExp, expGain));

                int afterExpLevel = myself.level.level;
                if(afterExpLevel > currentLevel)
                {
                    GameObject levelupGo = Pools.GetPool(Pools.LEVELUP_BOXES).GetObject();
                    LevelupBox levelup = levelupGo.GetComponent<LevelupBox>();
                    yield return StartCoroutine(levelup.AddStats(myself.characterName, currentStats, myself.level.stats));
                    levelupGo.SetActive(false);
                }
            }
            else
            {
                yield return new WaitForSeconds(1.5f);
            }
        }
        else
        {
            opponent.animator.SetBool("isDefending", false);
        }
        yield return new WaitForSeconds(0.5f); // Wait 0.5 seconds after finishing

        if (opponent.IsDead())
        {
            opponent.Kill();
        }
        this.renderer.sortingOrder = originalOrder;
        animator.SetBool("isIdle", true);
    }

    protected abstract IEnumerator QuickTimeEvent();
}
