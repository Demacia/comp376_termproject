﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleLabels : MonoBehaviour 
{
    private Label combatantName;
    private Label weaponClass;
    private Label hitChanceLabel;
    private Label dmgLabel;
    private Label cirtChanceLabel;

    public List<SpriteRenderer> renderers;

	void Awake () 
    {
        combatantName = this.transform.Find("Name/Label").GetComponent<Label>();
        weaponClass = this.transform.Find("Weapon/Label").GetComponent<Label>();
        hitChanceLabel = this.transform.Find("Stats/HitLabel").GetComponent<Label>();
        dmgLabel = this.transform.Find("Stats/DmgLabel").GetComponent<Label>();
        cirtChanceLabel = this.transform.Find("Stats/CritLabel").GetComponent<Label>();
	}
	
    public void SetCombatant(Combatant c)
    {
        combatantName.SetText(c.characterName);
        weaponClass.SetText(c.weapon.weaponClass);
    }

    public void SetLabels(int dmg, float hitChance, float critChance)
    {
        dmgLabel.SetText("Dmg-" + dmg);
        hitChanceLabel.SetText("Hit--" + (int)(hitChance*100));
        cirtChanceLabel.SetText("Crt--" + (int)(critChance*100));
    }
}
