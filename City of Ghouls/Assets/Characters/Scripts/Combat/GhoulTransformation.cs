using UnityEngine;
using System.Collections;

public class GhoulTransformation : MonoBehaviour
{
    private void Awake()
    {
        if (Application.loadedLevel > 2)
        {
            GetComponent<Animator>().SetBool("isGhoul", true);
        }
    }
}
