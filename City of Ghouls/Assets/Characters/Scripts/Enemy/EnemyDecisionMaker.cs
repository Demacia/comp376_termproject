﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Decides if the unit should wait/attack/use items/etc.
/// </summary>
[RequireComponent(typeof(GridMovement))]
public class EnemyDecisionMaker : MonoBehaviour 
{
    private GridMovement movement;
    private Combatant combatant;

    private void Awake()
    {
        combatant = GetComponent<Combatant>();
        movement = GetComponent<GridMovement>();
    }

    public void MovementFinished()
    {
        StartCoroutine(MakeDecision());
    }

    private IEnumerator MakeDecision()
    {
        GameObject target = Pools.GetPool(Pools.TARGETS).GetObject();
        target.transform.position = this.transform.position;
        yield return new WaitForSeconds(0.5f);
        target.SetActive(false);

        if(combatant != null)
        {
            combatant.SelectTarget();
        }
        else
        {
            movement.Wait();
        }
    }
}
