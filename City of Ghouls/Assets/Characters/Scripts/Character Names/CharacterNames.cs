﻿using UnityEngine;
using System.Collections;

public static class CharacterNames
{
    public static string mainCharacter = "Kaneki";
	public static string mainCharacterGhoul = "Hybrid Kaneki";
    public static string organDonor = "Rize";
    public static string bestFriendForever = "Hide";
    public static string mcLover = "Touka";
    public static string youngGirl = "Hinami";
    public static string manager = "Manager";
    public static string femaleFriend = "Female Worker";
    public static string maleFriend = "Male Friend";
    public static string mother = "Miss Fueguchi";
    public static string finalBoss = "Jason";
    public static string police = "Amon";
	public static string policeMinion = "Counter Ghoul";
    public static string ghoul = "Ghoul";
}
