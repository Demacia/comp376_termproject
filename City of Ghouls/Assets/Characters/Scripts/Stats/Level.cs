﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Level : MonoBehaviour 
{
    public int level;
    public int exp;
    public Stats stats;
    public string key { get; set; }

    public void Awake()
    {
        key = this.gameObject.name;
    }

    public void AddExperience(int amount)
    {
        exp += amount;
        while(exp > 99)
        {
            LevelUp();
            exp -= 100;
        }
    }

    private void LevelUp()
    {
        level++;
        stats.AddStats();
    }

    /// <summary>
    /// Adds the experience based on level difference, 50 for same level, +- 20 for each level difference (minimum 10).
    /// </summary>
    /// <returns>The experience gained.</returns>
	public int AddExperienceBasedOnLevel(int defenderLevel)
	{
		int baseExp = 50;
        int expDeduction = Mathf.Min((this.level - defenderLevel) * 20, 40);

        int expGain = baseExp - expDeduction;
		AddExperience(expGain);
        return expGain;
	}
}
