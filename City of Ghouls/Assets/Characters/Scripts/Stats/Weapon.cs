﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour 
{    
    public int range;
    public int attack;
    public int def;
    public string weaponClass;
    public float percentSuccess;
    
    GameObject enterKey;

    bool isPlaying = false,
         missed = false;
    
    float buttonPresses = 0f;
    
    public static int score = 0;
    Vector3 screenScale;
    float pitchIncrease = 0f;
    
	GameObject comboText;
	bool isTextEnabled;

    string QTEString;
    
    void Update()
    {
        if(isPlaying && Input.GetButtonDown("Select") && !missed)
        {            
            AudioManager.instance.PlayClick(pitchIncrease);
            pitchIncrease += 0.025f;
            buttonPresses++;
            
            QTEString = buttonPresses.ToString("0") + "X";
        }
        else if(!isPlaying)
        {
            pitchIncrease = 0f;     
        }
        else if(missed)
        {            
            QTEString = "MISS";
        }

		if(isPlaying && isTextEnabled)
		{
			comboText.GetComponent<GUIText>().text = QTEString;
		}
	}
    
    /// <summary>
    /// Coroutine that encapsulates the Offensive Minigame
    /// </summary>
    public IEnumerator PlayOffensiveMinigame()
    {
		EnableText();
        isPlaying = true;
        if(enterKey == null)
        {
            enterKey = GameObject.Find("Enter");
        }
        enterKey.GetComponent<Animator>().SetBool("isSpammable", true); 
        yield return new WaitForSeconds(3.0f);
        enterKey.GetComponent<Animator>().SetBool("isSpammable", false); 

        percentSuccess = Mathf.Max((buttonPresses / 10f), 0.1f);

        isPlaying = false;
        buttonPresses = 0;
		DisableText();
    }

	private void EnableText()
	{
		GUIText comboGuiText = CreateComboText();
		StyleText(comboGuiText);

		isTextEnabled = true;
	}

	private void DisableText()
	{
		QTEString = "";
		isTextEnabled = false;
		DestroyObject(comboText);
	}

	private GUIText CreateComboText()
	{
		comboText = new GameObject();
		comboText.name = "ComboTextCounter";
		comboText.AddComponent<GUIText>();

		return comboText.GetComponent<GUIText>();
	}

	private void StyleText(GUIText gt)
	{
		gt.color = Color.white;
		gt.fontSize = TextScaling.GetTextScale(50);
		gt.gameObject.transform.position = new Vector3(0.46f, 0.8f, 2.0f);
	}
    
    /// <summary>
    /// Coroutine that encapsulates the Defensive Minigame
    /// </summary>
    public IEnumerator PlayDefensiveMinigame()
    {   
		EnableText();
        isPlaying = true;  
        if(enterKey == null)
        {
            enterKey = GameObject.Find("Enter");
        }
        enterKey.GetComponent<Animator>().SetBool("isSpammable", true); 
        yield return new WaitForSeconds(3.0f);    
        enterKey.GetComponent<Animator>().SetBool("isSpammable", false); 
        
        percentSuccess = Mathf.Max(((100f - buttonPresses) / 100f), 0);
        isPlaying = false;
        buttonPresses = 0;
		DisableText();
    }

    /// <summary>
    /// Coroutine that encapsulates the Miss Scenario
    /// </summary>
    public IEnumerator Miss()
    {
        EnableText();
        isPlaying = true;
        missed = true;

        yield return new WaitForSeconds(1.0f);        
        
        percentSuccess = 0f;
        isPlaying = false;
        missed = false;
        DisableText();
    }
}
