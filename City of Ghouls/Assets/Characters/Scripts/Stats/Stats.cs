﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Stats
{
    public int totalHP, // Health (0 health means dead)
               currentHP,
               lck,     // Luck (2.5% chance of crit per luck)
               str,     // Attack strength (1 damage per strength)
               dex,     // Dodge and hit (Difference in dex adds to hit percent, 5% per difference)
               def;     // Defense (1 damage reduction per defense)

    public void AddStats()
    {
        float r = UnityEngine.Random.value;
        if(r < 0.6f)
        {
            totalHP++;
        }
        
        r = UnityEngine.Random.value;
        if(r < 0.6f)
        {
            lck++;
        }
        
        r = UnityEngine.Random.value;
        if(r < 0.6f)
        {
            str++;
        }
        
        r = UnityEngine.Random.value;
        if(r < 0.6f)
        {
            dex++;
        }

        r = UnityEngine.Random.value;
        if(r < 0.6f)
        {
            def++;
        }

		GiveFullHP();
    }

	private void GiveFullHP()
	{
		currentHP = totalHP;
	}

    public Stats Clone()
    {
        Stats ret = new Stats();
        ret.totalHP = this.totalHP;
        ret.currentHP = this.currentHP;
        ret.str = this.str;
        ret.dex = this.dex;
        ret.def = this.def;
        ret.lck = this.lck;
        return ret;
    }
}
