using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridMovement : MonoBehaviour 
{
    public int movementRange;
    public float moveSpeed;
    public bool moveAvailable;

    // The character position in grid coordintes
    public int Row;
    public int Col;

    // Ordered list to travel from one grid to another
    private LinkedList<Grid> path = new LinkedList<Grid>();

    public Combatant combatant { get; private set; }

    private Animator animator;
    private Transform sprite;

    void Awake()
    {
        moveAvailable = true;
        combatant = this.GetComponent<Combatant>();
        sprite = transform.Find("Sprite");
        animator = sprite.GetComponent<Animator>();
    }
    
    public void Select()
    {
        if(!moveAvailable)
        {
            return;
        }

        Grid grid = MapOutline.instance.GetGrid(Row, Col);
        int attackRange = 0;
        if(combatant != null)
        {
            if(combatant.weapon)
            attackRange = combatant.weapon.range;
        }
        grid.AvailableMovement(movementRange, attackRange);
    }
    
    public void UnSelect()
    {
        MapOutline.instance.Reset();
    }
    
    public void MoveTo(LinkedList<Grid> path)
    {
        if(path.Last.Value.IsActive)
        {
            this.path = path;

            MapOutline.instance.GetGrid(Row, Col).Occupant = null;
            path.Last.Value.Occupant = this;

            Director.instance.enabled = false;
            StartCoroutine(Step());
        }
        else
        {
            UnSelect();
        }

    }

    public void MoveAndAttack(LinkedList<Grid> path)
    {
        Grid thisGrid = MapOutline.instance.GetGrid(Row, Col);
        if(movementRange + combatant.weapon.range - 1 >= thisGrid.Distance(path.Last.Value))
        {
            this.path = path;

            MapOutline.instance.GetGrid(Row, Col).Occupant = null;
            path.Last.Value.Occupant = this;

            Director.instance.enabled = false;
            StartCoroutine(Step());
        }
        else
        {
            UnSelect();
        }
    }

    /// <summary>
    /// Step through the path (Coroutine)
    /// </summary>
    private IEnumerator Step()
    {
        animator.SetBool("isWalking", true);
        Quaternion originalRotation = sprite.localRotation;

        LinkedListNode<Grid> node = path.First;
        while(node != null)
        {
            Grid nextGrid = node.Value;
            if (nextGrid.Col < Col || nextGrid.Row < Row)
            {
                if(sprite.transform.localScale.x == -1)
                {
                    sprite.transform.localRotation = Quaternion.Euler(-30, 135, 0);
                }
                else
                {
                    sprite.transform.localRotation = Quaternion.Euler(30, 315, 0);
                }
            }
            else
            {
                if(sprite.transform.localScale.x == -1)
                {
                    sprite.transform.localRotation = Quaternion.Euler(30, 315, 0);
                }
                else
                {
                    sprite.transform.localRotation = Quaternion.Euler(-30, 135, 0);
                }
            }

            Vector3 currentPosition = this.transform.position;
            Vector3 nextPosition = nextGrid.transform.position;
            while(nextPosition.x != currentPosition.x || nextPosition.z != currentPosition.z)
            {
                currentPosition = Vector3.MoveTowards(currentPosition, nextPosition, moveSpeed);
                this.transform.position = currentPosition;
                yield return new WaitForEndOfFrame();
            }

            Row = nextGrid.Row;
            Col = nextGrid.Col;

            node = node.Next;
        }

        animator.SetBool("isWalking", false);
        sprite.transform.localRotation = originalRotation;
        BroadcastMessage("MovementFinished"); // Call all movement finished functions in the gameobject
    }

    private void MovementFinished()
    {
        MapOutline.instance.Reset();
    }
        
    public void Wait()
    {
        path.Clear();
        moveAvailable = false;
        MapOutline.instance.GetGrid(Row, Col).FireEvents();
        TurnControl.instance.EndCharacterTurn(this.tag);
    }

    public void CancelMovement()
    {
        MapOutline.instance.GetGrid(Row, Col).Occupant = null; // Reset the grid at the current position

        // Set the grid at the first position
        Grid grid = path.First.Value;
        Row = grid.Row;
        Col = grid.Col;
        grid.Occupant = this;
        this.transform.position = grid.transform.position;

        path.Clear();
        Director.instance.enabled = true;
        BroadcastMessage("UnSelect");
    }
}
