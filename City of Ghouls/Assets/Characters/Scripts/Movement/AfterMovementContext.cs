using UnityEngine;
using System;
using System.Collections.Generic;

[RequireComponent(typeof(GridMovement))]
public class AfterMovementContext : MonoBehaviour 
{
    private GridMovement gridMovement;
    private Dictionary<string, Action> actions = new Dictionary<string, Action>();

    private int selectedButton = 0;
    private List<GameObject> buttons = new List<GameObject>();

    private Combatant attacker;

	private void Awake () 
    {
        gridMovement = GetComponent<GridMovement>();
        attacker = GetComponent<Combatant>();
        this.enabled = false;
	}

    public void RegisterAction(string label, Action action)
    {
        actions.Add(label, action);
    }

    private void Update()
    {
        if(Input.GetButtonDown("Vertical"))
        {
            if(Input.GetAxis("Vertical") < 0)
            {
                buttons[selectedButton].guiTexture.color = Color.white; // Change the color of the selected action
                selectedButton = (selectedButton + 1) % buttons.Count;
                buttons[selectedButton].guiTexture.color = Color.red;
            }
            else
            {
                buttons[selectedButton].guiTexture.color = Color.white;
                selectedButton = (selectedButton - 1 + buttons.Count) % buttons.Count; // negative mods are weird
                buttons[selectedButton].guiTexture.color = Color.red;
            }
        }
        else if(Input.GetButtonDown("Select")) // Perform the action selected and reset everything else
        {
            string label = buttons[selectedButton].transform.GetChild(0).guiText.text;
            actions[label].Invoke();
            Reset();    
        }
        else if(Input.GetButtonDown("Unselect"))
        {
            gridMovement.CancelMovement();
            Reset();
        }
    }

    private void MovementFinished()
    {
        CheckAvailableActions();
        ShowMenu();
        this.enabled = true;
    }

    private void CheckAvailableActions()
    {
        if(attacker != null && attacker.AvailableAttacks())
        {
            actions.Add("Attack", attacker.SelectTarget);
        }

        actions.Add("Wait", gridMovement.Wait);
        actions.Add("Cancel", gridMovement.CancelMovement);
    }

    private void ShowMenu()
    {
        int index = 0;
        foreach(string s in actions.Keys)
        {
            GameObject button = Pools.GetPool(Pools.MENU_BUTTON).GetObject();
            button.SendMessage("SetButtonNumber", index++);
            button.SendMessage("SetText", s);
            buttons.Add(button);
        }

        buttons[0].guiTexture.color = Color.red;
    }

    private void Reset()
    {
        this.enabled = false;

        actions.Clear();
        
        for(int i = 0; i < buttons.Count; i++)
        {
            buttons[i].guiTexture.color = Color.white;
            buttons[i].SetActive(false);
        }
        selectedButton = 0;
        buttons.Clear();
    }
}
