﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class EventOnSelect : MonoBehaviour 
{
    public List<Action<GameObject, int>> action { get; private set; }

    private int turnCount = 1;

    private void Awake()
    {
        action = new List<Action<GameObject, int>>();
    }

    public void Select()
    {
        for (int i = 0; i < action.Count; i++)
        {
            action[i].Invoke(this.gameObject, turnCount);
        }
        turnCount++;
    }

    public void UnSelect()
    {
        turnCount--;
    }
}
