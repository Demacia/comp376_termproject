﻿using UnityEngine;
using System.Collections;

public class CutsceneFactory : MonoBehaviour 
{
	public static CutsceneFactory instance;

	public GameObject blackBandT;
	public GameObject blackBandB;

	GameObject blackBandTop;
	GameObject blackBandBottom;

	float screenWidth = Screen.width;
	float bandHeight = Screen.height/5;
	float bandHeightViewPort = 0.2f;
	float speed = 0.005f;
	
	Vector3 outOfViewPositionTop = new Vector3(0.0f, 1.05f, 0.0f);
	Vector3 outOfViewPositionBottom;
	
	Vector3 desiredPositionTop = new Vector3(0.0f, 0.85f, 0.0f);

	//CutScene
	bool creatingCutScene;
	bool endingCutScene;
    bool battleScene;

	void Awake()
	{
		if(instance != null)
		{
			Destroy(this.gameObject);
		}

		instance = this;
	}

	void Start () 
	{
		CreateBlackBands();
		SetBandSize();
	}

	void CreateBlackBands()
	{
		blackBandTop = (GameObject)GameObject.Instantiate(blackBandT, outOfViewPositionTop, Quaternion.identity);
		outOfViewPositionBottom = new Vector3(0.0f, -0.05f - bandHeightViewPort, 0.0f);
		blackBandBottom = (GameObject)GameObject.Instantiate(blackBandB, outOfViewPositionBottom, Quaternion.identity);
	}

	void SetBandSize()
	{
		GUITexture topBandGuiTexture = blackBandTop.GetComponent<GUITexture>();
		GUITexture botBandGuiTexture = blackBandBottom.GetComponent<GUITexture>();
		
		topBandGuiTexture.pixelInset = new Rect(0.0f, 0.0f, screenWidth, bandHeight);
		botBandGuiTexture.pixelInset = new Rect(0.0f, 0.0f, screenWidth, bandHeight);
	}

	public void CreateCutScene()
	{
		Vector3 bandPositionTop = blackBandTop.transform.position;
		Vector3 bandPositionBottom = blackBandBottom.transform.position;
		
		blackBandTop.transform.position = new Vector3(bandPositionTop.x, bandPositionTop.y - speed, bandPositionTop.z);
		blackBandBottom.transform.position = new Vector3(bandPositionBottom.x, bandPositionBottom.y + speed, bandPositionBottom.z);
		
		if(bandPositionTop.y <= desiredPositionTop.y)
		{
			creatingCutScene = false;
		}
	}
	
	public void EndCutScene()
	{
		Vector3 bandPositionTop = blackBandTop.transform.position;
		Vector3 bandPositionBottom = blackBandBottom.transform.position;
		
		blackBandTop.transform.position = new Vector3(bandPositionTop.x, bandPositionTop.y + speed, bandPositionTop.z);
		blackBandBottom.transform.position = new Vector3(bandPositionBottom.x, bandPositionBottom.y - speed, bandPositionBottom.z);
		
		if(bandPositionTop.y >= outOfViewPositionTop.y)
		{
			endingCutScene = false;
            if(!battleScene)
            {
                TurnControl.instance.EnableTurns();
            }
		}
	}

	void Update()
	{
		if(creatingCutScene)
		{
			CreateCutScene();
		}
		
		if(endingCutScene)
		{
			EndCutScene();
		}
	}

	public void StartCutScene(bool battleScene)
	{
        if(!battleScene)
        {
            TurnControl.instance.DisableTurns();
        }
		creatingCutScene = true;
        this.battleScene = battleScene;
	}
	
	public void FinishCutScene()
	{
		endingCutScene = true;
	}
}
