﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class DialogFactory : MonoBehaviour
{
	// Dialog Text
	public static DialogFactory instance;
	DialogText dialogText;
	Queue<Action> dialogQueue = new Queue<Action>();

	//DialogBox
	public GameObject dialogBox;
	Dialog[] dialogs;
	
	void Awake()
	{
		if(instance != null)
		{
			Destroy(this.gameObject);
		}
		instance = this;
	}

	void Start()
	{
		dialogText = DialogText.instance;
		dialogText.CreateTextBox(this.dialogBox);
	}

	//participantName MUST be the name of the variable assigned.
	public Dialog CreateParticipant(string participantName, GameObject character, string[] dialogText, bool showOnRight)
	{
		GameObject dialog = new GameObject(participantName, typeof(Dialog));
		dialog.GetComponent<Dialog>().SetParams(character, dialogText, showOnRight, participantName);
		return dialog.GetComponent<Dialog>();
	}

	public void SetDialogs(Dialog[] dialogs)
	{
		this.dialogs = dialogs;
	}

	public void StartCutScene()
	{
		AudioManager.instance.PlayDialog();
		Director.instance.enabled = false;
		CutsceneFactory.instance.StartCutScene(false);
		DialogText.instance.DisplayTextBox();
	}

	public void FinishCutScene()
	{
		CutsceneFactory.instance.FinishCutScene();
		DialogText.instance.RemoveText();
		DestroyDialog();
	}

	IEnumerator Waitfor(float time)
	{
		yield return new WaitForSeconds(time);
	}

	public void AddDialog(Action funct)
	{
		dialogQueue.Enqueue(funct);
	}

	public void StartDialog()
	{
		if(dialogQueue.Count > 0 && DialogText.displayNextDialog)
		{
			DialogText.displayNextDialog = false;
			dialogQueue.Dequeue()();
		}
	}

	public int GetDialogQueueCount()
	{
		return dialogQueue.Count;
	}

	void DestroyDialog()
	{
		dialogQueue.Clear();
	}

	public void ShowParticipant(int participantIndex, int textIndex, int talkingToParticipantIndex)
	{
		dialogs[participantIndex].ShowParticipant();
		ShowText(participantIndex, textIndex, talkingToParticipantIndex);
	}

	public void ShowText(int participantIndex, int textIndex, int talkingToParticipantIndex)
	{
		dialogText.DisplayText(dialogs[participantIndex].dialogText[textIndex],
		                       dialogs[talkingToParticipantIndex].GetParticipant(), 
		                       dialogs[participantIndex].GetParticipant(),
		                       dialogs[participantIndex].name);
	}
	
	public void RemoveParticipant(int participantIndex)
	{
		DialogText.instance.ResetText();
		dialogs[participantIndex].RemoveParticipant(IsLastAction());

		DialogText.displayNextDialog = true;
		if(IsLastAction())
		{
			FinishCutScene();
		}
		else
		{
			DialogFactory.instance.StartDialog();
		}
	}

	public void RemoveMultipleParticipants(int firstParticipantIndex, int secondParticipantIndex)
	{
		RemoveParticipant(firstParticipantIndex);
		RemoveParticipant(secondParticipantIndex);
	}

	bool IsLastAction()
	{
		return dialogQueue.Count == 0;
	}
}
