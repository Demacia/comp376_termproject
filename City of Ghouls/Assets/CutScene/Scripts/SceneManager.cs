﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SceneManager : MonoBehaviour 
{
	public static SceneManager instance;
	public static bool displayNextScene;
	
	bool fadeImageIn;
	bool fadeImageOut;

	GameObject defaultScreen;
	Scene currentScene;
	Scene previousScene;

	Vector3 imagePosition = new Vector3(0.5f, 0.5f, -1.0f);

	float imageHeight = 0.80f;
	float imageWidth = 1.0f;

	float fadeSpeed = 0.03f;

	bool imageReady;
	
	float zOffset = 0.0f;

	float currentTime;
	float timeToWait = 9.0f;
	bool isDone;

	void Awake()
	{
		if(instance != null)
		{
			Destroy(this.gameObject);
		}
		
		instance = this;
	}

	public void BringImageIn()
	{
		imageReady = false;

		Vector3 backgroundImagePosition = new Vector3(imagePosition.x, imagePosition.y, imagePosition.z + zOffset);
		currentScene.SetImage((GameObject)Instantiate(currentScene.GetImage(), backgroundImagePosition, Quaternion.identity));

		zOffset += 0.01f;

		currentScene.SetGuiTexture(currentScene.GetImage().GetComponent<GUITexture>());
		SetImageSize();
		Color imageColor = currentScene.GetGuiTexture().color;
		currentScene.GetGuiTexture().color = new Color(imageColor.r, imageColor.g, imageColor.b, 0.0f);
		fadeImageIn = true;
	}

	public void KickImageOut()
	{
		imageReady = false;
		fadeImageOut = true;
	}

	void Update()
	{
		if(fadeImageIn)
		{
			BringIn();
		}

		if(fadeImageOut)
		{
			KickOut();
		}

		if(imageReady)
		{
			if(Input.GetKeyDown (KeyCode.Return) && SceneFactory.instance.GetQueueScene().Count >= 0 && !isDone)
			{
				SetCurrentTime();
				if(!currentScene.IsTextDone())
				{
					DisplayText();
				}
				else
				{
					if(SceneFactory.instance.GetQueueScene().Count != 0)
					{
						DisplayNextScene();
					}
					else
					{
						isDone = true;
						KickImageOut();
					}
				}
			}

			if(currentTime + timeToWait <= Time.time && SceneFactory.instance.GetQueueScene().Count >= 0 && !isDone)
			{

				if(!currentScene.IsTextDone())
				{
					DisplayText();
				}
				else
				{
					if(SceneFactory.instance.GetQueueScene().Count != 0)
					{
						DisplayNextScene();
					}
					else
					{
						KickImageOut();
						isDone = true;
					}
				}
			}
		}
	}

	void DisplayNextScene()
	{
		SceneManager.displayNextScene = true;
		SceneFactory.instance.PlayScene();
	}
	
	void RemovePreviousImage()
	{
		if(previousScene != null)
		{
			Destroy (previousScene.GetImage());
		}
	}

	void RemoveCurrentImage()
	{
		SceneFactory.instance.FinishScene();
		Destroy (currentScene.GetImage());
	}

	public void BringIn()
	{
		Color currentColor = currentScene.GetGuiTexture().color;
		Color newColor = new Color(currentColor.r, currentColor.g, currentColor.b, currentColor.a + fadeSpeed);
		currentScene.GetGuiTexture().color = newColor;

		if(newColor.a >= 1.0f)
		{
			fadeImageIn = false;
			imageReady = true;
			RemovePreviousImage();
		}
	}

	public void KickOut()
	{
		Color currentColor = currentScene.GetGuiTexture().color;
		Color newColor = new Color(currentColor.r, currentColor.g, currentColor.b, currentColor.a - fadeSpeed);
		currentScene.GetGuiTexture().color = newColor;
		
		if(newColor.a <= 0.0f)
		{
			fadeImageOut = false;
			imageReady = true;
			RemoveCurrentImage ();
		}
	}

	void SetImageSize()
	{
		currentScene.GetImage().transform.localScale = new Vector3(imageWidth, imageHeight);
	}

	public void PlayScene(Scene scene)
	{
		int count = SceneFactory.instance.GetQueueScene().Count;
		SetPreviousScene();

		if(count > -1 && SceneManager.displayNextScene)
		{
			isDone = false;
			SceneManager.displayNextScene = false;
			currentScene = scene;
			BringImageIn();
		}
		DisplayText();
	}

	void SetCurrentTime()
	{
		currentTime = Time.time;
	}

	void SetPreviousScene()
	{
		if(currentScene != null)
		{
			previousScene = currentScene;
		}
	}

	void DisplayText()
	{
		SceneTextFactory.instance.DisplayText(currentScene.GetNextText());
		SetCurrentTime();
	}
}
