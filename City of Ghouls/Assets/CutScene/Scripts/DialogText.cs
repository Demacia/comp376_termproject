﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System;

public class DialogText : MonoBehaviour
{
	public static DialogText instance;
	public static bool displayNextDialog;
	
	GameObject textBox;
	GUIText textBoxText;
	GUIText charName;

	float textBoxWidth= Screen.width;
	float textBoxHeight= Screen.height/2;
	float textBoxWidthViewPort = 1.0f;
	float textBoxHeightViewPort = 0.125f;
	float speed = 0.01f;

	float currentTime;

	GUITexture textBoxGuiTexture;

	Vector3 desiredPositionTop = new Vector3(-0.5f, -0.45f, 1.5f);
	Vector3 outOfViewPositionBottom;

	bool bringInTextBox;
	bool removeTextBox;


	public bool textBoxReady;
	bool displayText;

	string textToDisplay;
	string charNameToDisplay;
	bool currentlyAnimatingText;
	bool canPressEnter;

	float textDisplayPause = 0.0075f;
	int maxLengthPerLine = 90;

	Color originalColor = new Color(0.5f, 0.5f, 0.5f, 0.5f);
	Color greyColor = new Color(0.2f, 0.2f, 0.2f, 0.5f);
	
	void Awake () 
	{
		if(instance != null)
		{
			Destroy (this.gameObject);
		}
		
		instance = this;

		displayNextDialog = true;
	}

	public void DisplayTextBox()
	{
		bringInTextBox = true;
		textBoxReady = false;
		textToDisplay = "";
	}
	

	public void RemoveText()
	{
		removeTextBox = true;
		textBoxReady = false;
		textToDisplay = "";
	}

	public void CreateTextBox(GameObject textBoxGO)
	{
		outOfViewPositionBottom = new Vector3(-0.5f, -1.0f - textBoxHeightViewPort, 1.5f);
		textBox = (GameObject)GameObject.Instantiate(textBoxGO, outOfViewPositionBottom, Quaternion.identity);

		textBoxGuiTexture = textBox.GetComponentInChildren<GUITexture>();

		textBoxGuiTexture.pixelInset = new Rect(0.0f, 0.0f, textBoxWidth, textBoxHeight);

		textBoxText = textBox.GetComponentInChildren<GUIText>();
		textBoxText.pixelOffset = new Vector2(textBoxWidth/2, textBoxHeight/1.75f);

		textBoxText.fontSize = TextScaling.GetTextScale();

        charName = textBox.transform.FindChild("NameBox").FindChild("NameText").GetComponent<GUIText>();
        charName.fontSize = TextScaling.GetTextScale();
	}
	
	void ShowTextBox()
	{
		Vector3 textPosition = textBox.transform.position;
		textBox.transform.position = new Vector3(textPosition.x, textPosition.y + speed, textPosition.z);

		if(textPosition.y >= desiredPositionTop.y)
		{
			bringInTextBox = false;
			textBoxReady = true;
		}
	}
	
	void MoveOutTextBox()
	{
		Vector3 textPosition = textBox.transform.position;
		textBox.transform.position = new Vector3(textPosition.x, textPosition.y - speed, textPosition.z);

		if(textPosition.y <= outOfViewPositionBottom.y)
		{
			removeTextBox = false;
			textBoxReady = true;
			textBoxText.text = "";
            charName.text = "";
		}
	}

	public void DisplayText(string text, GameObject participantBlur, GameObject participantUnblur, string charName)
	{
		Blur(participantBlur, participantUnblur);
		textToDisplay = text;

		//Make sure the text does not overlap the sides
		if(textToDisplay.Length > maxLengthPerLine)
		{
			ChopUpText();
		}

		ResetText();
		currentlyAnimatingText = true;
		displayText = true;
		charNameToDisplay = charName;
	}

	void Blur(GameObject participantBlur, GameObject participantUnblur)
	{
		participantBlur.GetComponent<GUITexture>().color = greyColor;
		participantUnblur.GetComponent<GUITexture>().color = originalColor;
	}

	public void ResetText()
	{
		textBoxText.text = "";
        charName.text = "";
	}

	IEnumerator ShowText()
	{
		charName.text = charNameToDisplay;
		for(int i = 0; i < textToDisplay.Length; i++)
		{
			ShowTextInSegments (textToDisplay[i]);
			yield return new WaitForSeconds(textDisplayPause);
		}

		CanDisplayNextText();
	}

	void ChopUpText()
	{
		textToDisplay =  Regex.Replace(textToDisplay, "(.{" + maxLengthPerLine + "})", "$1" + "-" + Environment.NewLine );
	}

	void ShowTextInSegments(char text)
	{
		textBoxText.text += text;
	}

	void FixedUpdate ()
	{		
		if(removeTextBox)
		{
			MoveOutTextBox();
			return;
		}

		if(bringInTextBox)
		{
			ShowTextBox();
			return;
		}

		if(textBoxReady && displayText)
		{
			StartCoroutine(ShowText());
			textBoxReady = false;
			canPressEnter = true;
		}

		if(canPressEnter && currentlyAnimatingText && Input.GetKeyDown(KeyCode.Return) && !bringInTextBox)
		{
			currentTime = Time.time;
			StopAllCoroutines();
			textBoxText.text = textToDisplay;

			canPressEnter = false;
			CanDisplayNextText();
		}

		if(!currentlyAnimatingText && textBoxReady && Input.GetKeyDown(KeyCode.Return) && !bringInTextBox && currentTime + 0.5f < Time.time)
		{
			if(DialogFactory.instance.GetDialogQueueCount() > 0)
			{
				DialogText.displayNextDialog = true;
				DialogFactory.instance.StartDialog();
			}
		}
	}

	void CanDisplayNextText()
	{
		currentlyAnimatingText = false;
		displayText = false;
		textBoxReady = true;
	}	
}
