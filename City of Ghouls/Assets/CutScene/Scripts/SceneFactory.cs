﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class SceneFactory : MonoBehaviour 
{
	public static SceneFactory instance;
	Queue<Scene> sceneQueue;
	Queue<Action> sceneFunctQueue;
	Queue<Action> sceneStepQueue;

	List<Scene> scenesToRemove;

	bool isCutscenePlaying;

	void Awake()
	{
		if(instance != null)
		{
			Destroy(this.gameObject);
		}
		instance = this;

		sceneQueue = new Queue<Scene>();
		sceneFunctQueue = new Queue<Action>();
		sceneStepQueue = new Queue<Action>();
		scenesToRemove = new List<Scene>();

		isCutscenePlaying = false;
	}

	void Start()
	{
		SceneManager.displayNextScene = true;
	}

    void Update()
    {
		if(Input.GetKeyDown(KeyCode.Q) && isCutscenePlaying)
        {
            SkipCutScene();
        }
    }

	public Queue<Scene> GetQueueScene()
	{
		return sceneQueue;
	}

	public Queue<Action> GetQueueFunct()
	{
		return sceneFunctQueue;
	}

	public Queue<Action> GetQueueStep()
	{
		return sceneStepQueue;
	}

	public void AddScene(Scene scene)
	{
		sceneQueue.Enqueue(scene);
		sceneFunctQueue.Enqueue(() => PlayScene());
	}

	public void AddStep(Action funct)
	{
		sceneStepQueue.Enqueue(funct);
	}

	public void PlayScene()
	{
		if(sceneQueue.Count > 0)
		{
			Scene scene = sceneQueue.Dequeue();
			scenesToRemove.Add(scene);
			SceneManager.instance.PlayScene(scene);
		}
	}

	public void FinishScene()
	{
		if(sceneStepQueue.Count > 0)
		{
			sceneStepQueue.Dequeue()();
		}
	}

	public void StartCutScene(bool battleScene)
	{
		isCutscenePlaying = true;
		AudioManager.instance.PlayScene();
        CutsceneFactory.instance.StartCutScene(battleScene);
	}
	
    void SkipCutScene()
    {
        while(sceneStepQueue.Count > 0)
        {
            sceneStepQueue.Dequeue()();
        }
        
        FinishCutScene();
        DestroyScenes();

    }

	public void FinishCutScene()
	{
		CutsceneFactory.instance.FinishCutScene();
		//DestroyScenes();
		SceneTextFactory.instance.EndText();
		isCutscenePlaying = false;
		AudioManager.instance.Stop();
	}

	void DestroyScenes()
	{
		foreach(Scene s in scenesToRemove)
		{
			Destroy (s.GetImage());
		}
	}
}
