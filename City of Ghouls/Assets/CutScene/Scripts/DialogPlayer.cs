﻿using UnityEngine;
using System.Collections;

public static class DialogPlayer
{
	//TeaShop
	//GOOD!
	public static void PlayTea()
	{
		DialogFactory df = DialogFactory.instance;
		string[] dialog1 = {"That was some good tea! It was good to see you " + CharacterNames.mainCharacter + ". Next time's on me!",
								"I think I saw some of our friends up ahead, you should go say hi!"};
		Dialog participant1 = df.CreateParticipant (CharacterNames.bestFriendForever, ResourceFactory.GetCutsceneItem ("BFF"), dialog1, false);

		string[] dialog2 = {"Pleasure's all mine. Take care " + CharacterNames.bestFriendForever + "."};
		Dialog participant2 = df.CreateParticipant (CharacterNames.mainCharacter, ResourceFactory.GetCutsceneItem ("MainCharacter"), dialog2, true);

		Dialog[] dialogs = {participant1, participant2};

		df.SetDialogs (dialogs);
		df.StartCutScene ();
		df.AddDialog (() => df.ShowParticipant (0, 0, 1));
		df.AddDialog (() => df.ShowParticipant (1, 0, 0));
		df.AddDialog (() => df.ShowText (0, 1, 1));
		df.AddDialog (() => df.RemoveMultipleParticipants (0, 1));

		df.StartDialog ();
	}

	//TeaShop
	//GOOD!
    public static void PlayKidnap()
	{
		DialogFactory df = DialogFactory.instance;
		string[] mcDialog = {"What is going on?! Get away from her!",
							 "That's enough... You're going down!"};
		Dialog mainCharacter = df.CreateParticipant (CharacterNames.mainCharacter, ResourceFactory.GetCutsceneItem ("MainCharacter"), mcDialog, false);

		string[] ghoulDialog = {"Go away if you want to live! You don't know who you're messing with... ",
								"Bring it on punk!"};
		Dialog ghoul = df.CreateParticipant (CharacterNames.ghoul, ResourceFactory.GetCutsceneItem ("Ghoul"), ghoulDialog, true);

		string[] donorDialog = {"PLEASE!!! HELP ME!!!"};
		Dialog donor = df.CreateParticipant (CharacterNames.organDonor, ResourceFactory.GetCutsceneItem ("OrganDonor"), donorDialog, true);

		Dialog[] dialogs = {mainCharacter, ghoul, donor};

		df.SetDialogs (dialogs);
		df.StartCutScene ();
		df.AddDialog (() => df.ShowParticipant (2, 0, 0));
		df.AddDialog (() => df.RemoveParticipant (2));
		df.AddDialog (() => df.ShowParticipant (0, 0, 1));
		df.AddDialog (() => df.ShowParticipant (1, 0, 0));
		df.AddDialog (() => df.RemoveParticipant (1));
		df.AddDialog (() => df.ShowText (0, 1, 1));
		df.AddDialog (() => df.ShowParticipant (1, 1, 0));
		df.AddDialog (() => df.RemoveMultipleParticipants (0, 1));

		df.StartDialog ();
	}

	//TeaShop
	//DONE!
    public static void PlayKidnap2()
	{
		DialogFactory df = DialogFactory.instance;
		string[] mcDialog = {"What was that?", 
			"He didn't look human to me.",
			"A Ghoul?! They're monsters!!!",
			"What?! You can transform?! Get away from me!",
			"I better run towards that exit!"};
		Dialog mainCharacter = df.CreateParticipant (CharacterNames.mainCharacter, ResourceFactory.GetCutsceneItem ("MainCharacter"), mcDialog, false);

		string[] donorDialog = {"Thank you so much for saving me.", 
			"That was a Ghoul! You identify them by their red pupils and black sclera.", 
			"Well you see, I am a Ghoul myself. But, I like to stay in my human form.",
			"No wait you don't understand... I don't harm humans!!!"};
		Dialog donor = df.CreateParticipant (CharacterNames.organDonor, ResourceFactory.GetCutsceneItem ("OrganDonor"), donorDialog, true);

		Dialog[] dialogs = {mainCharacter, donor};

		df.SetDialogs (dialogs);
		df.StartCutScene ();
		df.AddDialog (() => df.ShowParticipant (0, 0, 1));
		df.AddDialog (() => df.ShowParticipant (1, 0, 0));
		df.AddDialog (() => df.ShowText (0, 1, 1));
		df.AddDialog (() => df.ShowText (1, 1, 0));
		df.AddDialog (() => df.ShowText (0, 2, 1));
		df.AddDialog (() => df.ShowText (1, 2, 0));
		df.AddDialog (() => df.ShowText (0, 3, 1));
		df.AddDialog (() => df.ShowText (1, 3, 0));
		df.AddDialog (() => df.ShowText (0, 4, 1));

		df.AddDialog (() => df.RemoveMultipleParticipants (0, 1));

		df.StartDialog ();
	}

	//The grass thingy
	//GOOD!
    public static void PlayLover()
	{
		DialogFactory df = DialogFactory.instance;

		string[] mcDialog = {"There's a lot of Ghouls. And they're not from the Ghouligans. Looks like I'm in trouble...",
			"That's good to hear! Let's do this!"};
		Dialog mainCharacter = df.CreateParticipant (CharacterNames.mainCharacter, ResourceFactory.GetCutsceneItem ("MainCharacter"), mcDialog, false);

		string[] mcLoverDialog = {CharacterNames.mainCharacter + ", my name is " + CharacterNames.mcLover + ".",
			"I heard you wanted to become a Ghouligan so I came to find you. I'm from the Ghouligans too.",
			"These Ghouls have been causing trouble... I heard they were looking for you because you attacked one of their gang members.",
			"Don't worry though, I won't let them hurt you! I'm right by you."};
		Dialog mcLover = df.CreateParticipant (CharacterNames.mcLover, ResourceFactory.GetCutsceneItem ("MCLover"), mcLoverDialog, true);

		string[] dialog1 = {"I'm here too buddy!"};
		Dialog participant1 = df.CreateParticipant (CharacterNames.bestFriendForever, ResourceFactory.GetCutsceneItem ("BFF"), dialog1, true);

		string[] ghoulDialog = {"There he is! Get him and his puny friends!"};
		Dialog ghoul = df.CreateParticipant (CharacterNames.ghoul, ResourceFactory.GetCutsceneItem ("Ghoul"), ghoulDialog, true);

		Dialog[] dialogs = {mainCharacter, mcLover, participant1, ghoul};

		df.SetDialogs (dialogs);
		df.StartCutScene ();
		df.AddDialog (() => df.ShowParticipant (0, 0, 1));
		df.AddDialog (() => df.ShowParticipant (1, 0, 0));
		df.AddDialog (() => df.ShowText (1, 1, 0));
		df.AddDialog (() => df.ShowText (1, 2, 0));
		df.AddDialog (() => df.ShowText (1, 3, 0));
		df.AddDialog (() => df.ShowText (0, 1, 1));
		df.AddDialog (() => df.RemoveParticipant (1));
		df.AddDialog (() => df.ShowParticipant (2, 0, 0));
		df.AddDialog (() => df.RemoveParticipant (2));
		df.AddDialog (() => df.ShowParticipant (3, 0, 0));
		df.AddDialog (() => df.RemoveMultipleParticipants (0, 3));

		df.StartDialog ();
	}

	//End of grass thingy
	//DONE!
    public static void PlayCFG()
	{
		DialogFactory df = DialogFactory.instance;
		string[] managerDialog = {CharacterNames.mainCharacter + " is that you?", 
			"Why it is! I've heard so much about you. See, I'm a Ghouligan too, and now you're one of us.",
			"I just wanted to warn you... Be careful of the Ghouls, I've heard rumours that they are trying to seek revenge for what you've done.",
			"But being a Ghouligan also means you should be careful of the Counter Ghoul Force...", 
			"They will stop at nothing to hunt down every Ghoul they see.",
			"They always come to our dwellings and cause us pain and sorrow... Our safe zone is just up ahead."};
		Dialog manager = df.CreateParticipant (CharacterNames.manager, ResourceFactory.GetCutsceneItem ("Manager"), managerDialog, false);

		string[] mcDialog = {"Ghouligans should no longer live in fear. I will face them myself."};
		Dialog mainCharacter = df.CreateParticipant (CharacterNames.mainCharacter, ResourceFactory.GetCutsceneItem ("MainCharacter"), mcDialog, true);

		Dialog[] dialogs = {manager, mainCharacter};

		df.SetDialogs (dialogs);
		df.StartCutScene ();
		df.AddDialog (() => df.ShowParticipant (0, 0, 1));
		df.AddDialog (() => df.ShowText (0, 1, 1));
		df.AddDialog (() => df.ShowText (0, 2, 1));
		df.AddDialog (() => df.ShowText (0, 3, 1));
		df.AddDialog (() => df.ShowText (0, 4, 1));
		df.AddDialog (() => df.ShowText (0, 5, 1));
		df.AddDialog (() => df.ShowParticipant (1, 0, 0));
		df.AddDialog (() => df.RemoveMultipleParticipants (0, 1));

		df.StartDialog ();
	}

	//The kill CFG
    public static void PlayKillCFG()
	{
		DialogFactory df = DialogFactory.instance;
		string[] mcDialog = {"It's you! You're the one who turned me into a Ghoul...", 
			"Well, I... You know... Did what I had to do.",
			"I never got to thank you for saving my life. Without your organs I wouldn't have been here..."};
		Dialog mainCharacter = df.CreateParticipant (CharacterNames.mainCharacter, ResourceFactory.GetCutsceneItem ("MainCharacter"), mcDialog, false);

		string[] femaleFriendDialog = {"Hi " + CharacterNames.mainCharacter + "...",
			"I know you're upset for what happened, but it was really not my fault.",
			"I didn't mean to scare you, or harm you! I'm a Ghouligan you know.",
			"I was being attacked by those no good Ghouls, and if it wasn't for you I wouldn't be here...",
			"Hehehe, I'm glad you did.",
			"I'm glad I did. Now come help me fight off those Counter Ghoul Force officers. They're raiding our safe zones!",
			"But watch out for Amon... He's the most elite officer of all. Getting to him might be a real struggle."};
		Dialog femaleFriend = df.CreateParticipant (CharacterNames.organDonor, ResourceFactory.GetCutsceneItem ("OrganDonor"), femaleFriendDialog, true);
		Dialog[] dialogs = {mainCharacter, femaleFriend};
		
		df.SetDialogs (dialogs);
		df.StartCutScene ();
		df.AddDialog (() => df.ShowParticipant (0, 0, 1));
		df.AddDialog (() => df.ShowParticipant (1, 0, 0));
		df.AddDialog (() => df.ShowText (1, 1, 0));
		df.AddDialog (() => df.ShowText (1, 2, 0));
		df.AddDialog (() => df.ShowText (1, 3, 0));
		df.AddDialog (() => df.ShowText (0, 1, 1));
		df.AddDialog (() => df.ShowText (1, 4, 0));
		df.AddDialog (() => df.ShowText (0, 2, 1));
		df.AddDialog (() => df.ShowText (1, 5, 0));
		df.AddDialog (() => df.ShowText (1, 6, 0));

		df.AddDialog (() => df.RemoveMultipleParticipants (0, 1));
		
		df.StartDialog ();
	}
	

	//MC gets captured by the final boss
    public static void PlayMCCaptured()
	{
		DialogFactory df = DialogFactory.instance;

		string[] mcDialog = {"Hey " + CharacterNames.bestFriendForever + ". Really good to see you again.",
			"I need some human interaction.",
			"This Ghouligan life is really hard. Everyone is out there to get you.",
			"Wh... Who's that?!"};
		Dialog mainCharacter = df.CreateParticipant (CharacterNames.mainCharacter.ToString(), ResourceFactory.GetCutsceneItem ("MainCharacter"), mcDialog, false);

		string[] bffDialog = {"I'm always here for you. You're my best friend!",
			"Not everyone. Don't forget your friends, we're going to protect you no matter what!",
			CharacterNames.mainCharacter.ToString() + "!!! NO!!!",
			"I HAVE TO SAVE HIM!"};
		Dialog bestFriend = df.CreateParticipant (CharacterNames.bestFriendForever, ResourceFactory.GetCutsceneItem ("BFF"), bffDialog, true);

		string[] bossDialog = {"Well, well, well. If it isn't the famous " + CharacterNames.mainCharacter.ToString() + ".", 
			"I bet you heard I was looking for you. You caused me some serious losses!",
			"You're going to pay for everything you did!",
			"Take him away boys!"};
		Dialog boss = df.CreateParticipant (CharacterNames.finalBoss, ResourceFactory.GetCutsceneItem ("FinalBoss"), bossDialog, false);

		Dialog[] dialogs = {mainCharacter, bestFriend, boss};

		df.SetDialogs (dialogs);
		df.StartCutScene ();
		df.AddDialog (() => df.ShowParticipant (0, 0, 1));
		df.AddDialog (() => df.ShowParticipant (1, 0, 0));
		df.AddDialog (() => df.ShowText (0, 1, 1));
		df.AddDialog (() => df.ShowText (0, 2, 1));
		df.AddDialog (() => df.ShowText (1, 1, 0));
		df.AddDialog (() => df.ShowText (0, 3, 1));
		df.AddDialog (() => df.RemoveParticipant (0));
		df.AddDialog (() => df.RemoveParticipant (1));
		df.AddDialog (() => df.ShowParticipant (2, 0, 1));
		df.AddDialog (() => df.ShowText (2, 1, 1));
		df.AddDialog (() => df.ShowText (2, 2, 1));
		df.AddDialog (() => df.ShowText (2, 3, 1));
		df.AddDialog (() => df.RemoveParticipant (2));
		df.AddDialog (() => df.ShowParticipant (1, 2, 1));
		df.AddDialog (() => df.ShowParticipant (1, 3, 1));
		df.AddDialog (() => df.RemoveParticipant (1));

		df.StartDialog ();
	}

	//Friends go save MC
    public static void ThirteenthDialog()
	{
		DialogFactory df = DialogFactory.instance;
		string[] mcLoverDialog = {CharacterNames.mainCharacter + " is in here somewhere.", 
			"Let's defeat these minions, find " + CharacterNames.mainCharacter + ", and get out of here!"};
		Dialog mcLover = df.CreateParticipant (CharacterNames.bestFriendForever, ResourceFactory.GetCutsceneItem ("BFF"), mcLoverDialog, false);

		Dialog[] dialogs = {mcLover};

		df.SetDialogs (dialogs);
		df.StartCutScene ();
		df.AddDialog (() => df.ShowParticipant (0, 0, 0));
		df.AddDialog (() => df.ShowText (0, 1, 0));
		df.AddDialog (() => df.RemoveParticipant (0));

		df.StartDialog ();
	}

	//Dialog between MC and boss before the 1v1
    public static void FourteenthDialog()
	{
		DialogFactory df = DialogFactory.instance;
		string[] bossDialog = {"Looks like your friends are here to save you.", "I'll have some fun and take care of you fools myself!"};
		Dialog boss = df.CreateParticipant (CharacterNames.finalBoss, ResourceFactory.GetCutsceneItem ("FinalBoss"), bossDialog, false);

		string[] mcDialog = {"You're not taking care of anyone! You will pay for your crimes!!!"};
		Dialog mainCharacter = df.CreateParticipant (CharacterNames.mainCharacter, ResourceFactory.GetCutsceneItem ("MainCharacter"), mcDialog, true);

		Dialog[] dialogs = {boss, mainCharacter};

		df.SetDialogs (dialogs);
		df.StartCutScene ();
		df.AddDialog (() => df.ShowParticipant (0, 0, 1));
		df.AddDialog (() => df.ShowText (0, 1, 1));
		df.AddDialog (() => df.ShowParticipant (1, 0, 0));
		df.AddDialog (() => df.RemoveMultipleParticipants (0, 1));

		df.StartDialog ();
	}

	public static void TutorialIntroduction()
	{
		DialogFactory dialogFactory = DialogFactory.instance;
		
		string[] participant1Dialog = {"Whe... Where am I?", 
			"Oh yeah, that's right! How's it going " + CharacterNames.bestFriendForever + "?", 
			"Great! How do we get out of here?"};
		Dialog participant1 = dialogFactory.CreateParticipant(CharacterNames.mainCharacter , ResourceFactory.GetCutsceneItem("MainCharacter"), participant1Dialog, false);
		
		string[] participant2Dialog = {"Hi, " + CharacterNames.mainCharacter + "!" , 
			"It's me, " + CharacterNames.bestFriendForever + ". Your best friend.",
			"Good, thanks! I'm here to help you out.",
			"Try moving the cursor with the arrow keys and selecting with z.",
			"You can control your movement like that!"};
		Dialog participant2 = dialogFactory.CreateParticipant(CharacterNames.bestFriendForever, ResourceFactory.GetCutsceneItem("BFF"), participant2Dialog, true);
		
		Dialog[] dialogs = {participant1, participant2};
		
		dialogFactory.SetDialogs(dialogs);
		dialogFactory.StartCutScene();
		dialogFactory.AddDialog(() => dialogFactory.ShowParticipant(0, 0, 1));
		dialogFactory.AddDialog(() => dialogFactory.ShowParticipant(1, 0, 1));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(1, 1, 0));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(0, 1, 1));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(1, 2, 0));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(0, 2, 1));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(1, 3, 0));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(1, 4, 0));
		
		dialogFactory.AddDialog(() => dialogFactory.RemoveMultipleParticipants(0, 1));
		
		dialogFactory.StartDialog();
	}

	public static void TutorialHowToMove()
	{
		DialogFactory dialogFactory = DialogFactory.instance;
		string[] participant1Dialog = {"Great job. Looks like you're getting the hang of it!",
			"You can move and attack within the blue squares.", 
			"Your attack range is the red squares.", 
			"If you walk a bit further up, you will see some bad guys. They look like trouble!",
			"You can attack them if they are in range.",
			"There's an exit up ahead on the right, let's go there! You will see the tiles turn yellow to signify an exit."};
		Dialog participant1 = dialogFactory.CreateParticipant(CharacterNames.bestFriendForever, ResourceFactory.GetCutsceneItem("BFF"), participant1Dialog, false);
		
		Dialog[] dialogs = {participant1};
		
		dialogFactory.SetDialogs(dialogs);
		dialogFactory.StartCutScene();
		dialogFactory.AddDialog(() => dialogFactory.ShowParticipant(0, 0, 0));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(0, 1, 0));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(0, 2, 0));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(0, 3, 0));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(0, 4, 0));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(0, 5, 0));
		dialogFactory.AddDialog(() => dialogFactory.RemoveParticipant(0));
		
		dialogFactory.StartDialog();
	}

	public static void TutorialMiniGame()
	{
		DialogFactory dialogFactory = DialogFactory.instance;
		string[] participant1Dialog = {"Looks like we're gonna have to fight our way through!",
			"While attacking, you can spam the 'Enter' button to get bonus attack damage!",
			"If you are being attacked, you can do the same! Only this time you will get bonus defense!",
			"Once defeating an enemy, you can kill him. This will bring your HP back to full.",
			"If you decide not to kill him, you'll gain some extra stats.",
			"Choose wisely!"};
		
		Dialog participant1 = dialogFactory.CreateParticipant(CharacterNames.bestFriendForever, ResourceFactory.GetCutsceneItem("BFF"), participant1Dialog, false);
		
		Dialog[] dialogs = {participant1};
		
		dialogFactory.SetDialogs(dialogs);
		dialogFactory.StartCutScene();
		dialogFactory.AddDialog(() => dialogFactory.ShowParticipant(0, 0, 0));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(0, 1, 0));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(0, 2, 0));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(0, 3, 0));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(0, 4, 0));
		dialogFactory.AddDialog(() => dialogFactory.ShowText(0, 5, 0));
		dialogFactory.AddDialog(() => dialogFactory.RemoveParticipant(0));
		
		dialogFactory.StartDialog();
	}
}