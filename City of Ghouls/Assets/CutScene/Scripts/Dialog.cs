﻿using UnityEngine;
using System.Collections.Generic;

public class Dialog : MonoBehaviour
{
	GameObject participant;
	public string[] dialogText;
	public bool isOnRight;

	float participantWidth= Screen.width/4.5f;
	float participantHeight= Screen.height/1.5f;
	float participantWidthViewPort = 0.23f;
	float speed = 0.01f;

	Vector3 outOfViewPositionRight = new Vector3(1.0f, 0.21f, 1.0f);
	Vector3 outOfViewPositionLeft;
	
	Vector3 desiredPositionRight = new Vector3(0.82f, 0.21f, 1.0f);
	Vector3 desiredPositionLeft;

	bool bringInFromRight;
	bool bringInFromLeft;
	bool removeFromRight;
	bool removeFromLeft;

	GUITexture participantGuiTexture;
	bool destroyParticipant;

	public void SetParams(GameObject participant, string[] dialogText, bool showOnRight, string name)
	{
		this.dialogText = dialogText;
		this.isOnRight = showOnRight;

		if(showOnRight)
		{
			this.participant = (GameObject)GameObject.Instantiate(participant, outOfViewPositionRight, Quaternion.identity);
		}
		else
		{
			outOfViewPositionLeft = new Vector3(0.0f - participantWidthViewPort, 0.21f, 1.0f);
			desiredPositionLeft = new Vector3(0.2f - participantWidthViewPort, 0.21f, 1.0f);
			this.participant = (GameObject)GameObject.Instantiate(participant, outOfViewPositionLeft, Quaternion.identity);
		}
		participantGuiTexture = this.participant.GetComponentInChildren<GUITexture>();
		participantGuiTexture.pixelInset = new Rect(0.0f, 0.0f, participantWidth, participantHeight);

		if(!showOnRight)
		{
			this.participant.transform.localScale = new Vector3(-0.4f, 0.2f, 1.0f);
		}
		else
		{
			this.participant.transform.localScale = new Vector3(-0.05f, 0.2f, 1.0f);
		}
	}

	public void ShowParticipant()
	{
		if(isOnRight)
		{
			bringInFromRight = true;
		}
		else
		{
			bringInFromLeft = true;
		}
	}

	public void RemoveParticipant(bool destroyParticipant)
	{
		this.destroyParticipant = destroyParticipant;
		if(isOnRight)
		{
			removeFromRight = true;
		}
		else
		{
			removeFromLeft = true;
		}
	}

	void ShowParticipantOnLeft()
	{
		Vector3 participantPosition = participant.transform.position;
		participant.transform.position = new Vector3(participantPosition.x + speed, participantPosition.y, participantPosition.z);
		
		if(participantPosition.x >= desiredPositionLeft.x)
		{
			bringInFromLeft = false;
		}
	}

	void ShowParticipantOnRight()
	{
		Vector3 participantPosition = participant.transform.position;
		participant.transform.position = new Vector3(participantPosition.x - speed, participantPosition.y, participantPosition.z);
		
		if(participantPosition.x <= desiredPositionRight.x)
		{
			bringInFromRight = false;
		}
	}

	void RemoveParticipantOnLeft()
	{
		Vector3 participantPosition = participant.transform.position;
		participant.transform.position = new Vector3(participantPosition.x - speed, participantPosition.y, participantPosition.z);
		
		if(participantPosition.x <= outOfViewPositionLeft.x)
		{
			removeFromLeft = false;
			DestroyGameObject();
		}
	}

	void DestroyGameObject()
	{
		if(destroyParticipant)
		{
			Destroy (participant);
			Destroy (this.gameObject);
		}
	}
	
	void RemoveParticipantOnRight()
	{
		Vector3 participantPosition = participant.transform.position;
		participant.transform.position = new Vector3(participantPosition.x + speed, participantPosition.y, participantPosition.z);
		
		if(participantPosition.x >= outOfViewPositionRight.x)
		{
			removeFromRight = false;
			DestroyGameObject();
		}
	}

	void Update()
	{
		if(bringInFromRight)
		{
			ShowParticipantOnRight();
		}
		
		if(bringInFromLeft)
		{
			ShowParticipantOnLeft();
		}
		
		if(removeFromRight)
		{
			RemoveParticipantOnRight();
		}
		
		if(removeFromLeft)
		{
			RemoveParticipantOnLeft();
		}
	}

	public GameObject GetParticipant()
	{
		return participant;
	}
}
