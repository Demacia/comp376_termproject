﻿using UnityEngine;
using System.Collections;

public static class ScenePlayer
{
//	static void Cutscenes()
//	{
//		SceneFactory sceneFactory = SceneFactory.instance;
//
//		//Cutscene where Rize reveals that she's a ghould to MC and tries to eat him. Both gets into a car accident.
//		string[] mcFleeSceneText = {"Seeing " +CharacterNames.instance.organDonor.ToString ()+ " transform into a ghoul in front of his eyes, " +CharacterNames.instance.mainCharacter.ToString ()+ " decides to flee as fast as possible."};
//		//Scene fleeBackground = new Scene(ImageManager.instance.fleeingImage, mcFleeSceneText);
//		string[] carSkidSceneText = {"The moment where both entered an intersection, a sound of skidding tires can be heard.",
//									 "The car failed to avoid the two and collided with them."};
//		//Scene carSkidBackground = new Scene(ImageManager.instance.carSkidImage, carSkidSceneText);							
//		string[] hospitalSceneText = {"After three months of recovery at a local hospital, ",
//									  CharacterNames.instance.mainCharacter.ToString ()+ " was told by the doctor that he was donated the organs of the young girl that was with him in the accident.",
//									  "It is revealed that " +CharacterNames.instance.organDonor.ToString ()+ " has died after the donation and " +CharacterNames.instance.mainCharacter.ToString ()+ " lives on as a half human, half ghoul known as a hybrid."};
//		//Scene hospitalBackground = new Scene(ImageManager.instance.hospitalImage, hospitalSceneText);
//
//		//Cutscene where MC decides to avenge the mother's death
//		string[] mcAvengeSceneText = {CharacterNames.instance.mainCharacter.ToString ()+ " wants to avenge " +CharacterNames.instance.mother.ToString ()+ "'s death and goes to find out who killed her.",
//									  CharacterNames.instance.mainCharacter.ToString ()+ " finds out that the head of the Counter Ghoul Force named " +CharacterNames.instance.police.ToString ()+ " killed " +CharacterNames.instance.mother.ToString ()+ ".",
//									  CharacterNames.instance.mainCharacter.ToString ()+ " later finds out that " +CharacterNames.instance.finalBoss.ToString ()+ ", one of the strongest ghouls told " +CharacterNames.instance.mother.ToString ()+ " to go where " +CharacterNames.instance.police.ToString ()+ " was and was in turn killed there."};
//		//Scene mcAvengeImage = new Scene(ImageManager.instance.avengeImage, mcAvengeSceneText);
//
//		string[] beforeFinalStageSceneText = {"Everyone who was close to " +CharacterNames.instance.mainCharacter.ToString ()+ " got together and planned out how to save him",
//											  "A hard challenge lies ahead.",
//											  "The task to defeat " +CharacterNames.instance.finalBoss.ToString ()+ " and rescuing " +CharacterNames.instance.mainCharacter.ToString ()+ " was put upon them.",
//											  "Everyone has decided to put their lives at stake."};
//		//Scene preparationsImage = new Scene(ImageManager.instance.prepImage, beforeFinalStageSceneText);
//
//		//PLAY CUTSCENE STUFF GOES HERE
//
//		//Need to find images for them
//	}

	public static void PlayTutorialEnding()
	{
		SceneFactory sceneFactory = SceneFactory.instance;
		
		string[] citySceneText = {"Until the year 2048, humans were considered on top of the food chain.", 
			"Now, however, there are humans who hunt other humans for food."};
		Scene cityBackground = new Scene(ResourceFactory.GetCutsceneItem("PrologueCity"), citySceneText);
		
		string[] ghoulSceneText = {"These beings look exactly like humans, but possess special powers.", 
			"They are considered monsters by the public and are being hunted down by the Counter Ghoul Force established by the government.", 
			"These beings hunt humans and are known as Ghouls.", 
			"These Ghouls must eat human flesh to survive, since other sources of nutrition do not nourish them."};
		Scene ghoulExample = new Scene(ResourceFactory.GetCutsceneItem("PrologueGhoul"), ghoulSceneText);
		
		string[] twoGhoulsSceneText = {"However, not all Ghouls are bad.", 
			"There exists 2 types of Ghouls.", 
			"Those who kill and enjoy doing it, and those who do their best to blend in with the humans.", 
			"This group of Ghouls is known as Ghouligans and only eat the dead."};
		Scene twoGroupsGhouls = new Scene(ResourceFactory.GetCutsceneItem("PrologueGhoulTypes"), twoGhoulsSceneText);
		
		string[] MCText = {"There is one Ghoul however...", 
			"That stands on the edge of the 3 groups and must find where he truly belongs.",
			"His story begins in a small town after going for tea with his best friend..."};
		Scene bestCharacterEVAR = new Scene(ResourceFactory.GetCutsceneItem("PrologueMainCharacter"), MCText);
		
		sceneFactory.StartCutScene(false);
		
		sceneFactory.AddScene(cityBackground);
		sceneFactory.AddScene(ghoulExample);
		sceneFactory.AddScene(twoGroupsGhouls);
		sceneFactory.AddScene(bestCharacterEVAR);
		sceneFactory.AddStep(() => sceneFactory.FinishCutScene());

		sceneFactory.PlayScene();
	}

    public static void PlayAccident()
	{
		SceneFactory sceneFactory = SceneFactory.instance;
		
		//Cutscene where Rize reveals that she's a ghould to MC and tries to eat him. Both gets into a car accident.
		string[] mcFleeSceneText = {"Seeing " + CharacterNames.organDonor + " transform into a ghoul in front of his eyes, " + 
			CharacterNames.mainCharacter + " decides to flee as fast as he can."};
		Scene fleeBackground = new Scene(ResourceFactory.GetCutsceneItem ("hospitalBackground"), mcFleeSceneText);

		string[] carSkidSceneText = {"Trying to convince " + CharacterNames.mainCharacter + " that she is good, " + CharacterNames.organDonor + " chases after him.",
			"Both being rather distracted, they do not realize they are running into an intersection... And the inevitable happens.",
			"A speeding car collides with both of them."};
		Scene carSkidBackground = new Scene(ResourceFactory.GetCutsceneItem ("carAccidentBackground"), carSkidSceneText);							

		string[] hospitalSceneText = {"After three months of recovery at a local hospital, ",
			CharacterNames.mainCharacter + " was told by the doctor that the young girl he was with donated one of her organs to save his life.",
			"However, the organ belonged to a Ghoul, and so it came at a cost.",
			CharacterNames.mainCharacter + " will live on as a hybrid; half human, half ghoul.",
			CharacterNames.mainCharacter + ", having a strong love for humans, decides to join the Ghouligans and help them with their cause.",
			"He and his best friend, " + CharacterNames.bestFriendForever + ", have decided to travel together and find where Ghouligans reside.",
			"But, trouble lies ahead..."};
		Scene hospitalBackground = new Scene(ResourceFactory.GetCutsceneItem ("organDonorBackground"), hospitalSceneText);
		
        sceneFactory.StartCutScene(false);
		
		sceneFactory.AddScene(fleeBackground);
		sceneFactory.AddScene(carSkidBackground);
		sceneFactory.AddScene(hospitalBackground);
		sceneFactory.AddStep(() => sceneFactory.FinishCutScene());
		
		sceneFactory.PlayScene();
	}

	//IGNORE THIS ITS BAD!!!!
    public static void PlayChapter2Ending()
	{
		SceneFactory sceneFactory = SceneFactory.instance;
		
		string[] mcAvengeSceneText = {CharacterNames.mainCharacter+ " wants to avenge " + CharacterNames.mother + "'s death and goes to find out who killed her.",
			CharacterNames.mainCharacter + " finds out that the head of the Counter Ghoul Force named " + CharacterNames.police + " was responsible for her death.",
			CharacterNames.mainCharacter + " later finds out that " +CharacterNames.finalBoss+ ", one of the strongest ghouls told " + CharacterNames.mother + " to go where " +CharacterNames.police + " was and was in turn killed there."};
		Scene mcAvengeImage = new Scene(ResourceFactory.GetCutsceneItem("PrologueCity"), mcAvengeSceneText);
		
        sceneFactory.StartCutScene(false);
		
		sceneFactory.AddScene(mcAvengeImage);
		sceneFactory.AddStep(() => sceneFactory.FinishCutScene());
		
		sceneFactory.PlayScene();
	}

	//IGNORE THIS ITS BAD!!!!
    public static void PlayChapter3Ending()
	{
		SceneFactory sceneFactory = SceneFactory.instance;

		string[] beforeFinalStageSceneText = {"Everyone who was close to " +CharacterNames.mainCharacter+ " got together and planned out how to save him",
			"A hard challenge lies ahead.",
			"The task to defeat " +CharacterNames.finalBoss+ " and rescuing " +CharacterNames.mainCharacter + " was put upon them.",
			"Everyone has decided to put their lives at stake."};
        Scene preparationsImage = new Scene(ResourceFactory.GetCutsceneItem("PrologueCity"), beforeFinalStageSceneText);
		
        sceneFactory.StartCutScene(false);
		
		sceneFactory.AddScene(preparationsImage);
		sceneFactory.AddStep(() => sceneFactory.FinishCutScene());
		
		sceneFactory.PlayScene();
	}

    public static void PlayChapter1()
	{
		SceneFactory sf = SceneFactory.instance;
		
		string[] carAccidentSceneText = {CharacterNames.mainCharacter + " got into a car accident.", 
			"In order to survive, a transplant must be done."};
		Scene carAccidentBackground = new Scene(ResourceFactory.GetCutsceneItem ("carAccidentBackground"), carAccidentSceneText);
		
		string[] organDonorSceneText = {"The donor is a ghouligan and decides to donate her liver to save his life.", 
			"She didn't mean any harm as ghouligans try to live within the human society."};
		Scene organDonorBackground = new Scene(ResourceFactory.GetCutsceneItem ("organDonorBackground"), organDonorSceneText);
		
		string[] hospitalSceneText = {CharacterNames.mainCharacter + " gets out of the hospital after several months of recovery.",
			"Knowing that he is now a ghoul, he decides to live a life as a ghouligan to not harm anyone especially his best friend."};
		Scene hospitalBackground = new Scene(ResourceFactory.GetCutsceneItem ("hospitalBackground"), hospitalSceneText);
		
        sf.StartCutScene (false);
		sf.AddScene (carAccidentBackground);
		sf.AddScene (organDonorBackground);
		sf.AddScene (hospitalBackground);
		sf.AddStep (() => sf.FinishCutScene ());
		sf.PlayScene ();
	}

    public static void PlayMCInTrouble()
	{
		SceneFactory sf = SceneFactory.instance;

		string[] jasonSceneText = {"After hearing about the heroic victory of " + CharacterNames.mainCharacter + ", " + CharacterNames.finalBoss + " knew where to look in order to find him.",
			CharacterNames.mainCharacter + " and his friends have split up and it was the perfect opportunity..."};
		Scene jasonBackground = new Scene(ResourceFactory.GetCutsceneItem ("jasonBackground"), jasonSceneText);

        sf.StartCutScene (false);
		sf.AddScene (jasonBackground);
		sf.AddStep (() => sf.FinishCutScene ());
		sf.PlayScene ();
	}

	public static void PlayBosses()
	{
		SceneFactory sf = SceneFactory.instance;
		
		string[] cgfSceneText = {"The special unit named Counter Ghoul Force are after " + CharacterNames.mainCharacter + " because of his desire to restore peace to the Ghouligans.",
			"They see him as a threat and are looking to get rid of him as soon as possible."};
		Scene cgfBackground = new Scene(ResourceFactory.GetCutsceneItem ("cgfBackground"), cgfSceneText);
		
		string[] jasonSceneText = {"On the other hand, " + CharacterNames.finalBoss + ", the leader of Ghouls has heard all about " + CharacterNames.mainCharacter + ".",
			CharacterNames.mainCharacter + "'s name has been spread all around town because of his heroic acts to restore peace to the Ghouligans.",
			"Fearing for his safety, " + CharacterNames.finalBoss + " has ordered a large group of Ghouls to find and capture " + CharacterNames.mainCharacter + "."};
		Scene jasonBackground = new Scene(ResourceFactory.GetCutsceneItem ("jasonBackground"), jasonSceneText);
		
		sf.StartCutScene (false);
		sf.AddScene (cgfBackground);
		sf.AddScene (jasonBackground);
        sf.AddStep (() => sf.FinishCutScene ());
        sf.PlayScene ();
    }
    
    public static void PlayKidnapped()
	{
		SceneFactory sf = SceneFactory.instance;

		string[] preparationsSceneText = {"After seeing that " + CharacterNames.mainCharacter + " has been captured by " + CharacterNames.finalBoss + ",",
			CharacterNames.bestFriendForever + " rushes to group all of  " + CharacterNames.mainCharacter + "'s friends.",
			"He knows that he will require a great force to rescue " + CharacterNames.mainCharacter + ".", 
			"Shortly after, all of " + CharacterNames.mainCharacter +  "'s friends rallied together in order to save their beloved friend.",
			"They tracked the footsteps of " + CharacterNames.finalBoss + " and his gang of Ghouls and entered their safe zone."};
		Scene preparationsBackground = new Scene(ResourceFactory.GetCutsceneItem ("preparationsBackground"), preparationsSceneText);

        sf.StartCutScene (false);
		sf.AddScene (preparationsBackground);
		sf.AddStep (() => sf.FinishCutScene ());
		sf.PlayScene ();
	}

	public static void PlayFinal()
	{
		SceneFactory sf = SceneFactory.instance;
		
		string[] preparationsSceneText = {"With the defeat of " + CharacterNames.finalBoss + ", " + CharacterNames.mainCharacter + " has finally restored peace to the Ghouligans.",
			"The evil Ghouls, having no one to look up to, have finally decided to look up to " + CharacterNames.mainCharacter + " and follow his footsteps.",
			"They vowed to never again attack humans and become part of the Ghouligans."};
		Scene preparationsBackground = new Scene(ResourceFactory.GetCutsceneItem ("PrologueMainCharacter"), preparationsSceneText);

		string[] jasonSceneText = {"With human killings on the decline, Ghouligans have finally prevailed and allowed the government to trust them."};
		Scene jasonBackground = new Scene(ResourceFactory.GetCutsceneItem ("PrologueCity"), jasonSceneText);

		
		sf.StartCutScene (false);
		sf.AddScene (preparationsBackground);
		sf.AddScene (jasonBackground);
		sf.AddStep (() => sf.FinishCutScene ());
		sf.PlayScene ();
	}

	public static void PlayNegativeFinal()
	{
		SceneFactory sf = SceneFactory.instance;

        string[] mcRampageText = {"After defeating " +CharacterNames.finalBoss+ ", and eating ghouls that he defeated along the way, "+CharacterNames.mainCharacter+" is unable to continue to surpress his inner ghoul powers."};
		Scene mcRampageBackground = new Scene(ResourceFactory.GetCutsceneItem ("mcRampageBackground"), mcRampageText);

		string[] fittestText = {"Being in this state, " +CharacterNames.mainCharacter+ " goes on a rampage in the city killing other powerless humans and ghouls.",
			"With " +CharacterNames.mainCharacter+ " on the run, the city has become a place where only the fittest survive."};
		Scene fittestBackground = new Scene(ResourceFactory.GetCutsceneItem ("PrologueCity"), fittestText);

		sf.StartCutScene (false);
		sf.AddScene (mcRampageBackground);
		sf.AddScene (fittestBackground);
		sf.AddStep (() => sf.FinishCutScene ());
		sf.PlayScene ();
	}
}
