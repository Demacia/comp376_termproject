﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Scene
{
	GameObject image;
	string[] text;
	int textIndex = 0;
	GUITexture texture;
	bool isBackgroundScene;

	public Scene(GameObject image, string[] text, bool isBackgroundScene = false)
	{
		this.image = image;
		this.text = text;
		this.isBackgroundScene = isBackgroundScene;
	}

	public string GetNextText()
	{
		if((this.textIndex + 1) > this.text.Length)
		{
			return "";
		}

		return this.text[this.textIndex++];
	}

	public bool IsBackgroundScene()
	{
		return isBackgroundScene;
	}

	public GameObject GetImage()
	{
		return this.image;
	}

	public void SetImage(GameObject image)
	{
		this.image = image;
	}

	public bool IsTextDone()
	{
		return (this.textIndex + 1 > this.text.Length);
	}

	public void SetGuiTexture(GUITexture texture)
	{
		this.texture = texture; 
	}

	public GUITexture GetGuiTexture()
	{
		return texture;
	}
}
