﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System;

public class SceneTextFactory : MonoBehaviour 
{
	public static SceneTextFactory instance;

	public GameObject sceneText;
	GameObject text;
	GUIText textGui;
	int maxLengthPerLine = 80;

	Vector3 textPosition = new Vector3(0.5f, 0.12f, 1.0f);

	void Awake()
	{
		if(instance != null)
		{
			Destroy(this.gameObject);
		}

		instance = this;
	}

	void Start()
	{
		text = (GameObject)Instantiate(sceneText, textPosition, Quaternion.identity);
		text.transform.localScale = new Vector3(0.8f, 0.3f);
		textGui = text.GetComponent<GUIText>();

		textGui.fontSize = TextScaling.GetTextScale();
	}

	public void DisplayText(string str)
	{
		textGui.text = ChopUpText(str);
	}

	public void EndText()
	{
		textGui.text = "";
	}

	string ChopUpText(string str)
	{
		return Regex.Replace(str, "(.{" + maxLengthPerLine + "})", "$1" + "-" + Environment.NewLine );
	}

}
