﻿using UnityEngine;
using System.Collections;

public class StatBox : MonoBehaviour 
{
    private GUIText nameLabel;
    private GUIText typeLabel;
    private GUIText hpLabel;

    private void Awake()
    {
        nameLabel = transform.Find("NameLabel").GetComponent<GUIText>();
        typeLabel = transform.Find("TypeLabel").GetComponent<GUIText>();
        hpLabel = transform.Find("HPLabel").GetComponent<GUIText>();

        // Calculate screen position
        Vector3 position = this.transform.position;
        position.y = (Screen.height - guiTexture.pixelInset.height - 10) / Screen.height;
        this.transform.position = position;
    }

    public void SetLabels(GridMovement character)
    {
        nameLabel.text = "Name: " + character.combatant.characterName;;
        typeLabel.text = "Type: " + character.combatant.weapon.weaponClass;
        hpLabel.text = "HP: " + character.combatant.level.stats.currentHP;
    }
}
