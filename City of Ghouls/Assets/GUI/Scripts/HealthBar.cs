﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour 
{
    float left, top, length;
    Texture2D texture;
    int currentHP, totalHP;
    Color barColor;

	private void Awake () 
    {
        texture = new Texture2D(1, 1);
        this.enabled = false;

        length = Screen.width * 0.25f;
        top = Screen.height * 0.9f;
        if(this.gameObject.name == "Attacker")
        {
            left = Screen.width * 0.1f;
        }
        else if(this.gameObject.name == "Defender")
        {
            left = Screen.width * 0.6f;
        }
        else
        {
            throw new UnityException("Cannot put health bar on this game object");
        }
	}

    public void SetHP(int currentHP, int totalHP)
    {
        this.currentHP = currentHP;
        this.totalHP = totalHP;
        float percentHP = (float)currentHP/totalHP;

        if(percentHP <= 0.0f)
        {
            barColor = Color.clear;
        }
        else if(percentHP <= 0.2f)
        {
            barColor = Color.red;
        }
        else if(percentHP <= 0.7f)
        {
            barColor = Color.yellow;
        }
        else
        {
            barColor = Color.green;
        }
    }
	
    public IEnumerator DeductHP(int damage)
    {
        while(damage > 0)
        {
            damage--;
            SetHP(currentHP - 1, totalHP);
            yield return new WaitForEndOfFrame();
        }
    }

	private void OnGUI () 
    {
        texture.SetPixel(0,0, Color.grey);
        texture.Apply();
        
        GUI.skin.box.normal.background = texture; 
        GUI.Box(new Rect(left, top, length, 30), "");

        texture.SetPixel(0,0, barColor);
        texture.Apply();      

        GUI.skin.box.normal.background = texture; 
        GUI.skin.box.normal.textColor = Color.black;  

        GUI.Box(new Rect(left, top, length * currentHP/totalHP, 30), "");

        texture.SetPixel(0,0, Color.clear);
        texture.Apply();
        
        GUI.skin.box.normal.background = texture; 
        GUI.Box(new Rect(left, top, length, 30), Mathf.Max(0,currentHP) + "/" + totalHP);
	}
}
