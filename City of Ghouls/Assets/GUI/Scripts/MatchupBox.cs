﻿using UnityEngine;
using System.Collections;

public class MatchupBox : MonoBehaviour 
{
    private GUIText attackerName;
    private GUIText attackerType;
    private GUIText hpLabel;
    private GUIText dmgLabel;
    private GUIText hitLabel;
    private GUIText critLabel;
    private GUIText defenderType;
    private GUIText defenderName;

	private void Awake()
    {
        attackerName = transform.Find("AttackerNameLabel").guiText;
        attackerType = transform.Find("AttackerTypeLabel").guiText;
        hpLabel = transform.Find("HPLabel").guiText;
        dmgLabel = transform.Find("DmgLabel").guiText;
        hitLabel = transform.Find("HitLabel").guiText;
        critLabel = transform.Find("CritLabel").guiText;
        defenderType = transform.Find("DefenderTypeLabel").guiText;
        defenderName = transform.Find("DefenderNameLabel").guiText;

        // Calculate screen position
        Vector3 position = Vector3.zero;
        position.y = (Screen.height - guiTexture.pixelInset.height - 10) / Screen.height;
        this.transform.position = position;
    }

    /// <summary>
    /// Sets the labels of the matchup box.
    /// </summary>
    /// <param name="combatants">Combatants (attacker at 0, defender at 1).</param>
    public void SetLabels(Combatant[] combatants)
    {
        Combatant attacker = combatants [0];
        Combatant defender = combatants [1];

        float advantage = BattleController.instance.ApplyRockPaperScizors(attacker.weapon, defender.weapon);

        attackerName.text = attacker.characterName;
        attackerType.text = attacker.weapon.weaponClass;

        hpLabel.text = defender.level.stats.currentHP + " - HP - " + attacker.level.stats.currentHP;

        int attDmg = BattleController.instance.CalculateDamage(attacker, defender, false);
        int defDmg = BattleController.instance.CalculateDamage(defender, attacker, false);
        dmgLabel.text = defDmg + " - Dmg - " + attDmg;

        int attackerHit = (int)(BattleController.instance.HitPercentage(attacker, defender) * 100);
        int defenderHit = (int)(BattleController.instance.HitPercentage(defender, attacker) * 100);

        int attackerCrit = (int)(BattleController.instance.CritPercentage(attacker) * 100);
        int defenderCrit = (int)(BattleController.instance.CritPercentage(defender) * 100);

        hitLabel.text = defenderHit + "% - Hit - " + attackerHit + "%";
        critLabel.text = defenderCrit + "% - Crit - " + attackerCrit + "%";

        defenderType.text = defender.weapon.weaponClass;
        defenderName.text = defender.characterName;
    }
}
