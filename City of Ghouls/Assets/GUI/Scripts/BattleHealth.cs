﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleHealth : MonoBehaviour 
{
    private const int HP_PER_ROW = 40;

    private List<GameObject> health;
    private int healthIndex;

    private Label label;

    private void Awake()
    {
        health = new List<GameObject>();
        label = this.transform.Find("Label").GetComponent<Label>();
    }

    public void SetHealth(int maxHP, int currentHP)
    {
        for(int i = 0; i < health.Count; i++)
        {
            health[i].SetActive(false);
        }
        health.Clear();

        label.SetText(currentHP.ToString());
        healthIndex = currentHP - 1;
        for(int i = 0; i < maxHP; i++)
        {
            if(i < currentHP)
            {
                GameObject healthTick = Pools.GetPool(Pools.HP_TICKS).GetObject();
                health.Add(healthTick);
            }
            else
            {
                GameObject dmgTick = Pools.GetPool(Pools.DMG_TICKS).GetObject();
                health.Add(dmgTick);
            }
        }

        float width = health[0].renderer.bounds.size.x;
        float height = health[0].renderer.bounds.size.y;

        Vector3 position = this.transform.position;
        if(maxHP < HP_PER_ROW)
        {
            position.x -= width * health.Count/2;
        }
        else
        {
            position.x -= width * HP_PER_ROW/2;
            position.y += height/2;
        }

        for(int i = 0; i < health.Count; i++)
        {
            if(i == 40)
            {
                position.y -= height;
                position.x = this.transform.position.x - width * HP_PER_ROW/2;
            }

            health[i].transform.position = position;
            position.x += width;
        }
    }

    public IEnumerator DecreaseHealth(int amount)
    {
        while(amount > 0 && healthIndex > -1)
        {
            amount--;

            // Replace the health tick with a damage tick
            Vector3 tickPosition = health[healthIndex].transform.position;
            health[healthIndex].SetActive(false);

            health[healthIndex] = Pools.GetPool(Pools.DMG_TICKS).GetObject();
            health[healthIndex].transform.position = tickPosition;
            healthIndex--;
            label.SetText((healthIndex + 1).ToString());

            yield return new WaitForFixedUpdate();
        }
    }
}
