﻿using UnityEngine;
using System.Collections;

public class LevelupBox : MonoBehaviour 
{
    private GUIText nameLabel;
    private GUIText hpLabel;
    private GUIText strLabel;
    private GUIText dexLabel;
    private GUIText defLabel;
    private GUIText lckLabel;

    private void Awake()
    {
        nameLabel = transform.Find("NameLabel").guiText;
        hpLabel = transform.Find("HPLabel").guiText;
        strLabel = transform.Find("StrLabel").guiText;
        dexLabel = transform.Find("DexLabel").guiText;
        defLabel = transform.Find("DefLabel").guiText;
        lckLabel = transform.Find("LckLabel").guiText;
        
        // Calculate screen position
        Vector3 position = Vector3.zero;
        position.x = (Screen.width - guiTexture.pixelInset.width - 25) / Screen.width;
        position.y = (Screen.height/2 - guiTexture.pixelInset.height*0.5f) / Screen.height;
        position.z = 0;
        this.transform.localPosition = position;
    }

    public IEnumerator AddStats(string characterName, Stats before, Stats after)
    {
        nameLabel.text = characterName;

        hpLabel.text = "HP: " + before.totalHP;
        hpLabel.color = Color.black;

        strLabel.text = "Str: " + before.str;
        strLabel.color = Color.black;

        dexLabel.text = "Dex: " + before.dex;
        dexLabel.color = Color.black;

        defLabel.text = "Def: " + before.def;
        defLabel.color = Color.black;

        lckLabel.text = "Lck: " + before.lck;
        lckLabel.color = Color.black;

        yield return new WaitForSeconds(0.5f);
        if(before.totalHP < after.totalHP)
        {
            hpLabel.color = Color.green;
            hpLabel.text = "HP: " + after.totalHP;
        }

        yield return new WaitForSeconds(0.5f);
        if(before.str < after.str)
        {
            strLabel.color = Color.green;
            strLabel.text = "Str: " + after.str;
        }

        yield return new WaitForSeconds(0.5f);
        if(before.dex < after.dex)
        {
            dexLabel.color = Color.green;
            dexLabel.text = "Dex: " + after.dex;
        }
        
        yield return new WaitForSeconds(0.5f);
        if(before.def < after.def)
        {
            defLabel.color = Color.green;
            defLabel.text = "Def: " + after.def;
        }

        yield return new WaitForSeconds(0.5f);
        if(before.lck < after.lck)
        {
            lckLabel.color = Color.green;
            lckLabel.text = "Lck: " + after.lck;
        }
        yield return new WaitForSeconds(0.5f);
    }
}
