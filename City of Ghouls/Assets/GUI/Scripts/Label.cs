﻿using UnityEngine;
using System.Collections;

public class Label : MonoBehaviour 
{
    protected TextMesh text;

	protected virtual void Awake () 
    {
        var parentSpriteRenderer = this.transform.parent.GetComponent<SpriteRenderer>();
        if( parentSpriteRenderer != null )
        {
            this.renderer.sortingLayerID = parentSpriteRenderer.sortingLayerID;
            this.renderer.sortingOrder = parentSpriteRenderer.sortingOrder + 10;
        }

        text = GetComponent<TextMesh>();
	}

    public virtual void SetText(string text)
    {
        this.text.text = text;
    }
}
