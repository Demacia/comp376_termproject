﻿using UnityEngine;
using System.Collections;

public class MenuButton : MonoBehaviour 
{
    private GUIText label;

	private void Awake () 
    {	
        label = GetComponentInChildren<GUIText>();
	}
	
    /// <summary>
    /// Change the button label
    /// </summary>
    /// <param name="text">Text.</param>
    public void SetText(string text)
    {
        label.text = text;
    }

    /// <summary>
    /// Reposition the button so that button 0 is on top.
    /// </summary>
    /// <param name="number">Number.</param>
    public void SetButtonNumber(int number)
    {
        Vector3 position = this.transform.position;
        position.y = 1 - ((number+1) * this.guiTexture.pixelInset.height/Screen.height);
        position.x = 1 - this.guiTexture.pixelInset.width/Screen.width;
        this.transform.position = position;
    }
}
