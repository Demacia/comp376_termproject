﻿using UnityEngine;
using System.Collections;

public class ExpBox : MonoBehaviour 
{
    private Label expLabel;
    private GameObject expBar;

	void Awake () 
    {
        expLabel = transform.Find("Label").GetComponent<Label>();
        expBar = transform.Find("Progress").gameObject;
        this.gameObject.SetActive(false);
	}
	
	public IEnumerator AddExp(int currentExp, int amount)
    {
        this.gameObject.SetActive(true);

        float progress = (float)currentExp / 100.0f;
        expBar.transform.localScale = new Vector3(progress, 1, 1);

        Vector3 position = expBar.transform.localPosition;
        position.x = (1 - progress) * -1.33f;
        expBar.transform.localPosition = position;

        while (amount > 0)
        {
            amount--;
            currentExp++;

            if(currentExp > 100)
            {
                currentExp = 0;
            }
            expLabel.SetText(currentExp.ToString());

            progress = (float)currentExp/100.0f;
            expBar.transform.localScale = new Vector3(progress, 1, 1);

            position = expBar.transform.localPosition;
            position.x = (1 - progress) * -1.33f;
            expBar.transform.localPosition = position;

            yield return new WaitForFixedUpdate();
        }
        yield return new WaitForSeconds(1.0f);

        this.gameObject.SetActive(false);
    }
}
