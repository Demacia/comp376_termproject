using UnityEngine;
using System.Collections;

public class OutlinedLabel : Label 
{
    public Color outlineColor;
    public GameObject label;

    private TextMesh[] outlines;

    protected override void Awake () 
    {
        base.Awake();
        outlines = new TextMesh[8];
        for (int i = 0; i < outlines.Length; i++)
        {
            GameObject go = Instantiate(label) as GameObject;
            go.name = "Outline";
            go.transform.parent = this.transform;
            go.transform.localPosition = GetDisplacement(i);
            go.transform.localScale = Vector3.one;

            outlines[i] = go.GetComponent<TextMesh>();
            outlines[i].color = outlineColor;
            outlines[i].text = text.text;

            outlines[i].renderer.sortingOrder = this.renderer.sortingOrder-1;
            outlines[i].renderer.sortingLayerID = this.renderer.sortingLayerID;
        }
    }

    public override void SetText(string text)
    {
        base.SetText(text);
        for (int i = 0; i < outlines.Length; i++)
        {
            outlines[i].text = text;
        }
    }

    private Vector3 GetDisplacement(int index)
    {
        switch (index)
        {
            case 0:
                return new Vector3(0, 0.3f);
            case 1:
                return new Vector3(0.3f, 0.3f);
            case 2:
                return new Vector3(0, 0.3f);
            case 3:
                return new Vector3(-0.3f, 0.3f);
            case 4:
                return new Vector3(-0.3f, 0);
            case 5:
                return new Vector3(-0.3f, -0.3f);
            case 6:
                return new Vector3(0, -0.3f);
            case 7:
                return new Vector3(0.3f, -0.3f);
            default:
                return new Vector3(0, 0);
        }
    }
}

