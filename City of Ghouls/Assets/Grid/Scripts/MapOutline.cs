﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Control the grid outline of the map
/// </summary>
public class MapOutline : MonoBehaviour 
{
    public struct Coordinates
    {
        public Coordinates(int Row, int Col)
        {
            this.Row = Row;
            this.Col = Col;
        }

        public int Row;
        public int Col;
    }

    public static MapOutline instance;

    // Holds all the grid tiles for quick access
    private Dictionary<Coordinates, Grid> grids = new Dictionary<Coordinates, Grid>();

    // Indicates if rendererers are active
    public bool OutlinesActive {get; private set;}

	private void Awake () 
    {
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            for(int i = 0; i < this.transform.childCount; i++)
            {
                Grid grid = this.transform.GetChild(i).GetComponent<Grid>();
                Coordinates c = new Coordinates(grid.Row, grid.Col);
                grids.Add(c, grid);
            }
            OutlinesActive = true;
        }
	}
	
    private void Update()
    {
        if(Input.GetButtonDown("ShowMap"))
        {
            OutlinesActive = !OutlinesActive;
            foreach(Grid g in grids.Values)
            {
                g.renderer.enabled = !g.renderer.enabled;
            }
        }
    }

    private void OnDestroy()
    {
        if(instance == this)
        {
            instance = null;
        }
    }

    public void Reset()
    {
        foreach(Grid g in grids.Values)
        {
            g.Reset();
        }
    }

    public Grid GetGrid(int row, int col)
    {
        Coordinates c = new Coordinates(row, col);
        Grid grid;
        if(grids.TryGetValue(c, out grid))
        {
            return grid;
        }
        else
        {
            return null;
        }
    }
}
