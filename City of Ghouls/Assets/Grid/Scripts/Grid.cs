﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A grid tile. Holds information on available movement and current occupants.
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class Grid : MonoBehaviour 
{
    public Sprite activeGrid;
    public Sprite outlineGrid;

    // Grid Co-ordinates
    public int Row;
    public int Col;

    public bool IsActive {get; private set;}
    public GridMovement Occupant;

    private SpriteRenderer spriteRenderer;
    public Color defaultColor { get; set; }

    private List<Action<GridMovement>> gridEvents = new List<Action<GridMovement>>();

    private void Awake()
    {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        defaultColor = spriteRenderer.color;
    }

    public void AddEvent(Action<GridMovement> e)
    {
        gridEvents.Add(e);
    }

    /// <summary>
    /// Highlight all tiles that can be reached within a range from this tile.
    /// </summary>
    /// <param name="range">Range.</param>
    public void AvailableMovement(int movementRange, int attackRange)
    {
        if(movementRange + attackRange >= 0)
        {
            List<Grid> adjacents = this.GetAdjacentGrids();
            adjacents.RemoveAll((Grid g) => g.IsActive); // If the tile is blue, then no need to check again
            if(movementRange >= 0)
            {
                this.Highlight(Color.blue);
                this.IsActive = true;
                for(int i = 0; i < adjacents.Count; i++)
                {
                    adjacents[i].AvailableMovement(movementRange - 1, attackRange);
                }
            }
            else if(attackRange >= 0)
            {
                this.Highlight(Color.red);
                this.IsActive = false;
                for(int i = 0; i < adjacents.Count; i++)
                {
                    adjacents[i].AvailableMovement(movementRange, attackRange - 1);
                }
            }
        }
    }

    public void Reset()
    {
        spriteRenderer.sprite = outlineGrid;
        spriteRenderer.color = defaultColor;
        IsActive = false;

        // Make sure the outlines go away if the map grid is disabled
        if(!MapOutline.instance.OutlinesActive)
        {
            this.renderer.enabled = false;
        }
    }

    /// <summary>
    /// Determines whether this grid tile is occupied.
    /// </summary>
    /// <returns><c>true</c> if this grid tile is occupied by a character; otherwise, <c>false</c>.</returns>
    public bool IsOccupied()
    {
        return Occupant != null;
    }

    public void FireEvents()
    {
        for(int i = 0; i < gridEvents.Count; i++)
        {
            gridEvents[i].Invoke(Occupant);
        }
    }
    
    private void Highlight(Color color)
    {
        spriteRenderer.sprite = activeGrid;
        spriteRenderer.color = color;
        
        // Show the movement range grid even if the map is disabled
        if(!MapOutline.instance.OutlinesActive)
        {
            this.renderer.enabled = true;
        }
    }

    public List<Grid> GetAdjacentGrids()
    {
        List<Grid> adjacents = new List<Grid>();

        MapOutline map = MapOutline.instance;
        Grid adjacent = map.GetGrid(Row + 1, Col);
        if(adjacent != null)
        {
            adjacents.Add(adjacent);
        }
        adjacent = map.GetGrid(Row - 1, Col);
        if(adjacent != null)
        {
            adjacents.Add(adjacent);
        }
        adjacent = map.GetGrid(Row, Col + 1);
        if(adjacent != null)
        {
            adjacents.Add(adjacent);
        }
        adjacent = map.GetGrid(Row, Col - 1);
        if(adjacent != null)
        {
            adjacents.Add(adjacent);
        }
        return adjacents;
    }

    public List<Grid> FindPath(Grid destination)
    {
        List<Grid> path = new List<Grid>();
        path.Add(this);

        if(destination == this)
        {
            return path;
        }

        Grid closestGrid = this;
        foreach(Grid g in this.GetAdjacentGrids())
        {
            if(g.Distance(destination) < closestGrid.Distance(destination))
            {
                closestGrid = g;
            }
            else if(g.Distance(destination) == closestGrid.Distance(destination) && closestGrid.IsOccupied() && !g.IsOccupied())
            {
                // Prioritize non occupied grids
                closestGrid = g;
            }
        }
        path.AddRange(closestGrid.FindPath(destination));

        return path;
    }

    /// <summary>
    /// Get the distance between this grid and grid g
    /// </summary>
    public int Distance(Grid g)
    {
        int distance = Mathf.Abs(this.Row - g.Row);
        distance += Mathf.Abs(this.Col - g.Col);
        return distance;
    }

    public void SetCoordinates(MapOutline.Coordinates c)
    {
        this.Row = c.Row;
        this.Col = c.Col;
    }
}
