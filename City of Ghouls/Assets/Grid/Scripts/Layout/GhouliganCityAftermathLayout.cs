using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GhouliganCityAftermathLayout : IGridLayout 
{
	private bool dialogOver = false;

	public override void InstantiateLevel()
	{
		// Players
		GameObject main = ResourceFactory.SpawnCharacter("PlayerCharacter", 4, 5);
		DisableCharacter.Disable(main);
		ResourceFactory.SpawnCharacter("BFF", -1, 5);
		
		//Enemies
		GameObject e_1 = ResourceFactory.SpawnCharacter("EnemyLancer", 6, 5);
		GameObject e_2 = ResourceFactory.SpawnCharacter("EnemyLancer", 6, 6);
		GameObject e_3 = ResourceFactory.SpawnCharacter("EnemyLancer", 6, 4);
		GameObject e_4 = ResourceFactory.SpawnCharacter("FinalBoss", 5, 5);
		GameObject e_5 = ResourceFactory.SpawnCharacter("EnemyLancer", 5, 3);
		GameObject e_6 = ResourceFactory.SpawnCharacter("EnemyLancer", 5, 7);

		DisableCharacter.Disable(e_1);
		DisableCharacter.Disable(e_2);
		DisableCharacter.Disable(e_3);
		DisableCharacter.Disable(e_4);
		DisableCharacter.Disable(e_5);
		DisableCharacter.Disable(e_6);

		ResourceFactory.LoadGlobalItems();
		
		ResourceFactory.SpawnDirector(0, 3);

		List<Grid> goalGrids = new List<Grid>();

		for (int i = 0; i < 5; i++)
		{
			for(int j = 0; j < 10; j++)
			{
				goalGrids.Add(MapOutline.instance.GetGrid(i, j));
			}
		}
		
		ReachTheGoal objective2 = new ReachTheGoal(goalGrids);
		TurnControl.instance.objectives.Add(objective2);
		
		for (int i = 0; i < goalGrids.Count; i++)
		{
			goalGrids[i].AddEvent(EndingCutscene);
			goalGrids[i].GetComponent<SpriteRenderer>().color = Color.yellow;
			goalGrids[i].defaultColor = Color.yellow;
		}
		
		StartCoroutine(PlaySceneDelayed());
	}
	
	private IEnumerator PlaySceneDelayed()
	{
		yield return new WaitForEndOfFrame(); // Wait for items to load
		DialogPlayer.PlayMCCaptured();
	}
	
	private void EndingCutscene(GridMovement m)
	{
		if (m.tag == "Player")
		{
			ScenePlayer.PlayKidnapped();
		}
	}
}
