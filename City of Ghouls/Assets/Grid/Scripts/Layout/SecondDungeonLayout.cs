using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SecondDungeonLayout : IGridLayout 
{
    
    public override void InstantiateLevel()
    {
        // Players
        GameObject mc = ResourceFactory.SpawnCharacter("PlayerCharacter", 1, 2);
		GameObject bff = ResourceFactory.SpawnCharacter("BFF", -1, 1);
		GameObject mcLover = ResourceFactory.SpawnCharacter("MCLover", -1, 2);
		GameObject od = ResourceFactory.SpawnCharacter("OrganDonor", -1, 3);
        
		DisableCharacter.LevelUp(new GameObject[]{mc, mcLover, od, bff}, 400);

        //Enemies
        ResourceFactory.SpawnCharacter("FinalBoss", 3, 2);
        
        ResourceFactory.LoadGlobalItems();
        
        ResourceFactory.SpawnDirector(0, 4);
        
        EliminateAllEnemies objective = new EliminateAllEnemies();
        TurnControl.instance.objectives.Add(objective);
        
        List<Grid> goalGrids = new List<Grid>();
        goalGrids.Add(MapOutline.instance.GetGrid(3, 2));
        
        ReachTheGoal objective2 = new ReachTheGoal(goalGrids);
        TurnControl.instance.objectives.Add(objective2);
        
        for (int i = 0; i < goalGrids.Count; i++)
        {
            goalGrids[i].AddEvent(EndingCutscene);
            goalGrids[i].GetComponent<SpriteRenderer>().color = Color.yellow;
            goalGrids[i].defaultColor = Color.yellow;
        }
        
        // Play the introduction dialogs once the cutscene system loads
        StartCoroutine(PlaySceneDelayed());
    }

	private IEnumerator PlaySceneDelayed()
	{
		yield return new WaitForEndOfFrame(); // Wait for items to load
		DialogPlayer.FourteenthDialog();
	}

	private void EndingCutscene(GridMovement m)
	{
		if (m.tag == "Player" && TurnControl.instance.AreAllEnemiesDead())
		{
            if(PlayerData.instance.IsGood())
            {
			    ScenePlayer.PlayFinal();
            }
            else
            {
                ScenePlayer.PlayNegativeFinal();
            }
		}
	}
}


