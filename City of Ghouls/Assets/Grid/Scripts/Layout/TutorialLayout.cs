using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialLayout : IGridLayout
{
    public override void InstantiateLevel()
    {
        GameObject main = ResourceFactory.SpawnCharacter("PlayerCharacter", 0, 0);
        ResourceFactory.SpawnCharacter("EnemyLancer", 3, 6);
        ResourceFactory.SpawnCharacter("EnemyLancer", 8, 11);
        
        ResourceFactory.LoadGlobalItems();
        
        ResourceFactory.SpawnDirector(0, 0);

		// On the first select of the PC, give him an explanation of the grid
		EventOnSelect e = main.AddComponent<EventOnSelect>();
		e.action.Add(HowToMove);
		e.action.Add(MiniGame);
        
        List<Grid> goalGrids = new List<Grid>();
        for (int i = 7; i < 10; i++)
        {
            for(int j = 9; j < 16; j++)
            {
                goalGrids.Add(MapOutline.instance.GetGrid(i, j));
            }
        }

        for (int i = 0; i < goalGrids.Count; i++)
        {
            goalGrids[i].AddEvent(EndingCutscene);
            goalGrids[i].GetComponent<SpriteRenderer>().color = Color.yellow;
            goalGrids[i].defaultColor = Color.yellow;
        }

        ReachTheGoal objective = new ReachTheGoal(goalGrids);
        TurnControl.instance.objectives.Add(objective);

        // Play the introduction dialogs once the cutscene system loads
        StartCoroutine(PlaySceneDelayed());
    }

	private IEnumerator PlaySceneDelayed()
	{
		yield return new WaitForEndOfFrame(); // Wait for items to load
		Introduction ();
	}

	private void Introduction()
	{
		DialogPlayer.TutorialIntroduction();
	}

    private void HowToMove(GameObject go, int turncount)
    {
		DialogPlayer.TutorialHowToMove();
		go.GetComponent<EventOnSelect>().action.Remove(HowToMove);
    }

	private void MiniGame(GameObject go, int turncount)
	{
        if (turncount < 2)
        {
            return;
        }

		DialogPlayer.TutorialMiniGame();
		go.GetComponent<EventOnSelect>().action.Remove(MiniGame);
    }
    
    private void EndingCutscene(GridMovement m)
    {
        if (m.tag == "Player")
        {
			ScenePlayer.PlayTutorialEnding();
        }
    }
}

