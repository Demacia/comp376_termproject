using System;
using UnityEngine;

public abstract class IGridLayout : MonoBehaviour
{
    /// <summary>
    /// Load the characters and other necessities onto the grid.
    /// </summary>
    public abstract void InstantiateLevel();
}

