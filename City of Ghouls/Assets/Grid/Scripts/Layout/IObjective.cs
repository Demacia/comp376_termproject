
public interface IObjective
{
    bool IsComplete();
}
