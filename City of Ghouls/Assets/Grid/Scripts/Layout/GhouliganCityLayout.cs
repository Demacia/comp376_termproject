﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GhouliganCityLayout : IGridLayout 
{
    public override void InstantiateLevel()
    {
        // Players
        ResourceFactory.SpawnCharacter("PlayerCharacter", 1, 5);
        ResourceFactory.SpawnCharacter("BFF", 0, 6);
        GameObject od = ResourceFactory.SpawnCharacter("OrganDonor", 0, 4);
		GameObject mcLover = ResourceFactory.SpawnCharacter("MCLover", 1, 7);
        
        //Enemies
		GameObject e_2 = ResourceFactory.SpawnCharacter("CounterGhoul", 5, 5);
		GameObject e_3 = ResourceFactory.SpawnCharacter("CounterGhoul", 5, 6);
		GameObject e_4 = ResourceFactory.SpawnCharacter("CounterGhoul", 5, 4);
		GameObject e_5 = ResourceFactory.SpawnCharacter("CounterGhoulBoss", 4, 5);
		GameObject e_6 = ResourceFactory.SpawnCharacter("CounterGhoul", 4, 3);
		GameObject e_7 = ResourceFactory.SpawnCharacter("CounterGhoul", 4, 7);

		DisableCharacter.LevelUp(new GameObject[]{e_2, e_3, e_4, e_5, e_6, e_7, od, mcLover}, 400);
        
        ResourceFactory.LoadGlobalItems();
        
        ResourceFactory.SpawnDirector(0, 3);
        
        EliminateAllEnemies objective = new EliminateAllEnemies();
        TurnControl.instance.objectives.Add(objective);
        
        List<Grid> goalGrids = new List<Grid>();
        for (int i = 5; i < 8; i++)
        {
            for(int j = 0; j < 2; j++)
            {
                goalGrids.Add(MapOutline.instance.GetGrid(i, j));
            }
        }
        
        ReachTheGoal objective2 = new ReachTheGoal(goalGrids);
        TurnControl.instance.objectives.Add(objective2);
        
        for (int i = 0; i < goalGrids.Count; i++)
        {
            goalGrids[i].AddEvent(EndingCutscene);
            goalGrids[i].GetComponent<SpriteRenderer>().color = Color.yellow;
            goalGrids[i].defaultColor = Color.yellow;
        }
        
        StartCoroutine(PlaySceneDelayed());
    }

	private IEnumerator PlaySceneDelayed()
	{
		yield return new WaitForEndOfFrame(); // Wait for items to load
		DialogPlayer.PlayKillCFG();
	}

	private void EndingCutscene(GridMovement m)
	{
		if (m.tag == "Player" && TurnControl.instance.AreAllEnemiesDead())
		{
			ScenePlayer.PlayMCInTrouble();
		}
	}
}
