using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FirstDungeonLayout : IGridLayout 
{
    
    public override void InstantiateLevel()
    {
        // Players
        ResourceFactory.SpawnCharacter("BFF", 0, 6);
        ResourceFactory.SpawnCharacter("OrganDonor", 0, 4);
        ResourceFactory.SpawnCharacter("MCLover", 0, 5);
        
        //Enemies
		GameObject e_1 = ResourceFactory.SpawnCharacter("EnemyLancer", 4, 4);
		GameObject e_2 = ResourceFactory.SpawnCharacter("EnemyLancer", 4, 7);
		GameObject e_3 = ResourceFactory.SpawnCharacter("EnemyLancer", 4, 6);
		GameObject e_4 = ResourceFactory.SpawnCharacter("EnemyLancer", 5, 5);
		GameObject e_5 = ResourceFactory.SpawnCharacter("EnemyLancer", 4, 3);
        
		DisableCharacter.LevelUp(new GameObject[]{e_1, e_2, e_3, e_4, e_5}, 600);

        ResourceFactory.LoadGlobalItems();
        
        ResourceFactory.SpawnDirector(0, 4);
        
        EliminateAllEnemies objective = new EliminateAllEnemies();
        TurnControl.instance.objectives.Add(objective);
        
        List<Grid> goalGrids = new List<Grid>();
        for (int i = 0; i < 10; i++)
        {
            goalGrids.Add(MapOutline.instance.GetGrid(8, i));
        }
        
        ReachTheGoal objective2 = new ReachTheGoal(goalGrids);
        TurnControl.instance.objectives.Add(objective2);
        
        for (int i = 0; i < goalGrids.Count; i++)
        {
            goalGrids[i].GetComponent<SpriteRenderer>().color = Color.yellow;
            goalGrids[i].defaultColor = Color.yellow;
        }
        
        // Play the introduction dialogs once the cutscene system loads
        StartCoroutine(PlaySceneDelayed());
    }

	private IEnumerator PlaySceneDelayed()
	{
		yield return new WaitForEndOfFrame(); // Wait for items to load
		DialogPlayer.ThirteenthDialog();
	}
	
}

