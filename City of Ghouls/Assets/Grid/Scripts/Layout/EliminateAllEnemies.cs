
using System.Collections.Generic;

public class EliminateAllEnemies : IObjective
{
    public bool IsComplete()
    {
        List<GridMovement> enemies;
        TurnControl.instance.characters.TryGetValue("Enemy", out enemies);
        return enemies.Count < 1;
    }
}
