
using System.Collections.Generic;

public class ReachTheGoal : IObjective
{
    List<Grid> goalGrids;

    private ReachTheGoal()
    {
    }

    public ReachTheGoal(List<Grid> goalGrids)
    {
        this.goalGrids = goalGrids;
    }

    public bool IsComplete()
    {
        for (int i = 0; i < goalGrids.Count; i++)
        {
            if(goalGrids[i].IsOccupied())
            {
                if(goalGrids[i].Occupant.tag == "Player")
                {
                    return true;
                }
            }
        }
        return false;
    }
}