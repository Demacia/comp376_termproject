﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BasementTrainingLayout : IGridLayout 
{
	bool allEnemiesDead = false;

    public override void InstantiateLevel()
    {
        // Players
        ResourceFactory.SpawnCharacter("PlayerCharacter", 1, 3);
        GameObject bff = ResourceFactory.SpawnCharacter("BFF", 0, 4);
		GameObject mcLover = ResourceFactory.SpawnCharacter("MCLover", 0, 2);

		GameObject oldLady = ResourceFactory.SpawnCharacter("OldLady", 6, 0);
		DisableCharacter.Disable(oldLady);

        //Enemies
        GameObject e_1 = ResourceFactory.SpawnCharacter("EnemyLancer", 4, 0);
		GameObject e_2 = ResourceFactory.SpawnCharacter("EnemyLancer", 3, 3);
		GameObject e_3 = ResourceFactory.SpawnCharacter("EnemyLancer", 6, 3);
		GameObject e_4 = ResourceFactory.SpawnCharacter("EnemyLancer", 4, 2);
		GameObject e_5 = ResourceFactory.SpawnCharacter("EnemyLancer", 4, 4);
		GameObject e_6 = ResourceFactory.SpawnCharacter("EnemyLancer", 4, 6);

		DisableCharacter.LevelUp(new GameObject[]{e_1, e_2, e_3, e_4, e_5, e_6, bff, mcLover}, 200);
        
        ResourceFactory.LoadGlobalItems();
        
        ResourceFactory.SpawnDirector(0, 3);

        EliminateAllEnemies objective = new EliminateAllEnemies();
        TurnControl.instance.objectives.Add(objective);

		List<Grid> goalGrids = new List<Grid>();
		for (int i = 0 ; i < 7; i++)
		{
			goalGrids.Add(MapOutline.instance.GetGrid(6, i));
		}

		ReachTheGoal objective2 = new ReachTheGoal(goalGrids);
		TurnControl.instance.objectives.Add(objective2);

		for (int i = 0; i < goalGrids.Count; i++)
		{
			goalGrids[i].AddEvent(EndingCutscene);
			goalGrids[i].GetComponent<SpriteRenderer>().color = Color.yellow;
			goalGrids[i].defaultColor = Color.yellow;
		}

		// Play the introduction dialogs once the cutscene system loads
		StartCoroutine(PlaySceneDelayed());
	}
	
	private IEnumerator PlaySceneDelayed()
	{
		yield return new WaitForEndOfFrame(); // Wait for items to load
		DialogPlayer.PlayLover();
	}

	private void EndingCutscene(GridMovement m)
	{
		if (m.tag == "Player" && TurnControl.instance.AreAllEnemiesDead())
		{
			ScenePlayer.PlayBosses();
		}
	}

	void Update()
	{
		if(!allEnemiesDead && TurnControl.instance.AreAllEnemiesDead())
		{
			allEnemiesDead = true;
			DialogPlayer.PlayCFG();
		}
	}
}
