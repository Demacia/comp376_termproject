using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TeaShopLayout : IGridLayout 
{
	bool allEnemiesDead = false;

    public override void InstantiateLevel()
    {
        GameObject main = ResourceFactory.SpawnCharacter("PlayerCharacter", 1, 5);
		GameObject organDonor = ResourceFactory.SpawnCharacter("OrganDonor", 1, 10);
		DisableCharacter.Disable(organDonor);
		GameObject bff = ResourceFactory.SpawnCharacter("BFF", 1, 1);
		DisableCharacter.Disable(bff);

        // Enemies
        ResourceFactory.SpawnCharacter("EnemyLancer", -2, 10);

        ResourceFactory.LoadGlobalItems();
        
        ResourceFactory.SpawnDirector(1, 5);

        List<Grid> goalGrids = new List<Grid>();
        for (int i = 4; i < 8; i++)
        {
            for(int j = 8; j < 13; j++)
            {
                goalGrids.Add(MapOutline.instance.GetGrid(i, j));
            }
        }

        // On the first select of the PC, give him an explanation of the grid
        EventOnSelect e = main.AddComponent<EventOnSelect>();
		e.action.Add(Kidnap);

        EliminateAllEnemies objective = new EliminateAllEnemies();
        TurnControl.instance.objectives.Add(objective);

        ReachTheGoal objective2 = new ReachTheGoal(goalGrids);
        TurnControl.instance.objectives.Add(objective2);

        for (int i = 0; i < goalGrids.Count; i++)
        {
            goalGrids[i].AddEvent(EndingCutscene);
            goalGrids[i].GetComponent<SpriteRenderer>().color = Color.yellow;
            goalGrids[i].defaultColor = Color.yellow;
        }

        // Play the introduction dialogs once the cutscene system loads
        StartCoroutine(PlaySceneDelayed());
    }
    
    private IEnumerator PlaySceneDelayed()
    {
        yield return new WaitForEndOfFrame(); // Wait for items to load
		DialogPlayer.PlayTea();
    }

	private void Kidnap(GameObject go, int turncount)
    {
		if (turncount < 2)
		{
			return;
		}

		DialogPlayer.PlayKidnap();
		go.GetComponent<EventOnSelect>().action.Remove(Kidnap);
    }

    private void EndingCutscene(GridMovement m)
    {
		if (m.tag == "Player" && TurnControl.instance.AreAllEnemiesDead())
        {
			ScenePlayer.PlayAccident();
        }
    }

	void Update()
	{
		if(!allEnemiesDead && TurnControl.instance.AreAllEnemiesDead())
		{
			allEnemiesDead = true;
			DialogPlayer.PlayKidnap2();
		}
	}
}

