﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(IGridLayout))]
public class GridFactory : MonoBehaviour 
{
    public GameObject grid;
    public float gridSize;

	private void Start()
    {
        GameObject mapOutline = new GameObject("MapOutline");
        CreateGrids(mapOutline);

        mapOutline.AddComponent<MapOutline>();
        GetComponent<IGridLayout>().InstantiateLevel();
        Destroy(this);
    }

    private void CreateGrids(GameObject outline)
    {
        for(int i = 0; i < this.transform.childCount; i++)
        {
            GameObject gridArea = this.transform.GetChild(i).gameObject;
            if(gridArea.tag != "GridGeneration")
            {
                throw new UnityException("Illegal child object on GridFactory");
            }

            Bounds bounds = gridArea.collider.bounds;

            // Get the area for the grids
            int numCols = (int)((bounds.max.x - bounds.min.x)/gridSize);
            int numRows = (int)((bounds.max.z - bounds.min.z)/gridSize);

            // Start at the lower left corner
            Vector3 position = bounds.min;
            position.x -= gridSize/2;
            for(int j = 0; j < numCols; j++)
            {
                position.x += gridSize;
                position.z = bounds.min.z - gridSize/2;
                for(int k = 0; k < numRows; k++)
                {
                    position.z += gridSize;

                    // Instantiate the grid and parent it to the outline
                    GameObject g = Instantiate(grid) as GameObject;
                    g.transform.parent = outline.transform;
                    g.transform.position = position;

                    MapOutline.Coordinates coordinates = FindCoordinates(position, bounds);
                    g.SendMessage("SetCoordinates", coordinates);
                }
            }

            Destroy(gridArea);
        }
    }

    private MapOutline.Coordinates FindCoordinates(Vector3 newGridPosition, Bounds bounds)
    {
        // Find a neighboring grid so that the coordinates can be used
        GameObject other = GameObject.FindWithTag("Grid");
        int row = 0;
        int col = 0;
        if(other != null)
        {
            Grid otherGrid = other.GetComponent<Grid>();
            row = otherGrid.Row;
            col = otherGrid.Col;

            col += (int)((newGridPosition.x - otherGrid.transform.position.x)/gridSize);
            row += (int)((newGridPosition.z - otherGrid.transform.position.z)/gridSize);
        }

        return new MapOutline.Coordinates(row, col);
    }
}
